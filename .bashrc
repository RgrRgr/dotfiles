# No double entries in the shell history.
export HISTCONTROL="$HISTCONTROL erasedups:ignoreboth"

#Other
export HISTSIZE=10000                               # Maximum events for internal history
export SAVEHIST=$HISTSIZE                           # Maximum events for history file
export HISTFILE="$XDG_STATE_HOME/bash/history"      # History file location
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export W3M_DIR="$XDG_STATE_HOME/w3m"
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel

### less ###
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# Other
export EDITOR=nvim
# export PAGER=nvimpager

# Do not overwrite files when redirecting output by default.
set -o noclobber

exec fish
