local has_treesitter, ts = pcall(require, 'vim.treesitter')
local _, query = pcall(require, 'vim.treesitter.query')

local ls = require("luasnip")
-- some shorthands...
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local l = require("luasnip.extras").lambda
local rep = require("luasnip.extras").rep
local p = require("luasnip.extras").partial
local m = require("luasnip.extras").match
local n = require("luasnip.extras").nonempty
local dl = require("luasnip.extras").dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local types = require("luasnip.util.types")
local conds = require("luasnip.extras.conditions")
local conds_expand = require("luasnip.extras.conditions.expand")

local MATH_ENVIRONMENTS = {
  displaymath = true,
  eqnarray = true,
  equation = true,
  math = true,
  array = true,
}
local MATH_NODES = {
  displayed_equation = true,
  inline_formula = true,
  math_environment = true,
}

local COMMENT = {
  ['comment'] = true,
  ['line_comment'] = true,
  ['block_comment'] = true,
  ['comment_environment'] = true,
}

local function get_node_at_cursor()
  local cursor = vim.api.nvim_win_get_cursor(0)
  local cursor_range = { cursor[1] - 1, cursor[2] }
  local buf = vim.api.nvim_get_current_buf()
  local ok, parser = pcall(ts.get_parser, buf, 'latex')
  if not ok or not parser then return end
  local root_tree = parser:parse()[1]
  local root = root_tree and root_tree:root()

  if not root then return end

  return root:named_descendant_for_range(cursor_range[1], cursor_range[2], cursor_range[1], cursor_range[2])
end

local function is_comment()
  local node = get_node_at_cursor()
  while node do
    if COMMENT[node:type()] then
      return true
    end
    node = node:parent()
  end
  return false
end

local function is_math()
  if has_treesitter then
    local buf = vim.api.nvim_get_current_buf()
    local node = get_node_at_cursor()
    while node do
      if MATH_NODES[node:type()] then
        return true
      end
      if node:type() == 'environment' then
        local begin = node:child(0)
        local names = begin and begin:field('name')

        if names and names[1] and MATH_ENVIRONMENTS[query.get_node_text(names[1], buf):gsub('[%s*]', '')] then
          return true
        end
      end
      node = node:parent()
    end
    return false
  end
end

local rec_item
rec_item = function()
	return sn(nil, {
		c(1, {
			-- Order is important, sn(...) first would cause infinite loop of expansion.
			t({""}),
			sn(nil, {t({"", "\t\\item "}), i(1), d(2, rec_item, {})}),
		}),
	});
end

local slice_string = function(str, start)
	local i, j = str:find("%d+", start)
	if (i == nil) then
		return {
			first = str:sub(start, #str)
		}
	end
	return {
		first = str:sub(start, i - 1),
		second = str:sub(j + 1, #str),
		start = j + 1,
	}
end

local smart_range = function(args, snip, old_state)
	local text = args[1][1] -- v_{p}(x_{1})
	if (text == nil or text == "") then
		return sn(nil, {t({"failed"})})
	end
	local strings = {}
	local index = 1
	while(true) do
		local res = slice_string(text, index)
		table.insert(strings, res.first)
		if (res.second ~= nil) then
			index = res.start
		else
			break
		end
	end
	local nodes = {}
	for k, v in ipairs(strings) do
		if (k == 1) then
			table.insert(nodes, f(function(args, snip, usr_args) return v end, {}))
			if(#(strings) > 1) then
				table.insert(nodes, i(1, "n"))
			end
		else
			table.insert(nodes, f(function(args) return v end, {}))
			if (k < #(strings)) then
				table.insert(nodes, f(function(args) return args[1][1] end, {1}))
			end
		end
	end
	return sn(nil, nodes)
end

return {
	s("ls", {
		t({"\\begin{itemize}", "\t\\item "}),
		i(1),
		d(2, rec_item, {}),
		t({"", "\\end{itemize}"}),
	}),

	s({
		trig = "enum",
		name = "Enumarate-Umgebung"
	}, {
		t({"\\begin{enumerate}"}),
		c(1, {
			t({"[label=(\\arabic*)]"}),
			t({"[label=(\\roman*)]"}),
			t({"[label=(\\alph*)]"}),
		}),
		t({"", "\t\\item "}),
		i(2),
		d(3, rec_item, {}),
		t({"", "\\end{enumerate}"}),
	}),

	s({
		trig = "lm",
		name = "Inline maths",
	}, {
		t({"\\("}), i(1), t({"\\)"})
	}),

	s({
		trig = "beg",
		name = "Begin",
	}, {
		t({"\\begin{"}),
		i(1),
		t({"}", "\t"}),
		i(2),
		t({"", "\\end{"}),
		f(function(args, s, f) return args[1][1] end, {1}),
		t({"}"}),
	}),

	s({
		trig = "eqnn",
		name = "Equation no number"
	}, {
		t({"\\begin{equation*}", "\t"}),
		i(1),
		t({"", "\\end{equation*}"})
	}),

	s({
		trig = "eq",
		name = "Equation"
	}, {
		t({"\\begin{equation}", "\t"}),
		i(1),
		t({"", "\\end{equation}"})
	}),

	s({
		trig = "alnn",
		name = "Align no number"
	}, {
		t({"\\begin{align*}", "\t"}),
		i(1),
		t({"", "\\end{align*}"})
	}),

	s({
		trig = "al",
		name = "Align"
	}, {
		t({"\\begin{align}", "\t"}),
		i(1),
		t({"", "\\end{align}"})
	}),

	s({
		trig = "sec",
		name = "Section"
	}, {
		t({"\\section{"}),
		i(1),
		t({"}", ""})
	}),

	s({
		trig = "ssec",
		name = "Sub-Section"
	}, {
		t({"\\subsection{"}),
		i(1),
		t({"}", ""})
	}),

	s({
		trig = "sssec",
		name = "Sub-Section"
	}, {
		t({"\\subsubsection{"}),
		i(1),
		t({"}", ""})
	}),

	s({
		trig = "^^",
		name = "Supscript",
		wordTrig = false,
		snippetType = "autosnippet",
	}, {
		t({"^{"}),
		i(1),
		t({"}"})
	}, {
		condition = is_math
	}),

	s({
		trig = "__",
		name = "Subscript",
		wordTrig = false,
		snippetType = "autosnippet",
	}, {
		t({"_{"}),
		i(1),
		t({"}"})
	}, {
		condition = is_math
	}),

	s({
		trig = "in",
		name = "In",
		snippetType = "autosnippet",
	}, {
		t({"\\in"}),
	}, {
		condition = is_math
	}),

	s({
		trig = "dots",
		name = "Dots",
		snippetType = "autosnippet",
	}, {
		t({"\\dots"}),
	}, {
		condition = is_math
	}),

	s({
		trig = "cdot",
		name = "Centered dot",
		snippetType = "autosnippet",
	}, {
		t({"\\cdot"}),
	}, {
		condition = is_math
	}),

	s({
		trig = "frac",
		name = "Fraction",
		snippetType = "autosnippet",
	}, {
		t({"\\frac{"}),
		i(1),
		t({"}{"}),
		i(2),
		t({"}"})
	}, {
		condition = is_math
	}),

	s({
		trig = "binom",
		name = "Binomial coefficient",
		snippetType = "autosnippet",
	}, {
		t({"\\binom{"}),
		i(1, "n"),
		t({"}{"}),
		i(2, "k"),
		t({"} "}),
	}, {
		condition = is_math
	}),

	s({
		trig = "rarr",
		name = "Implication",
		snippetType = "autosnippet",
	}, {
		t({"\\Rightarrow"}),
	}, {
		condition = is_math
	}),

	s({
		trig = "lrarr",
		name = "Equivalence",
		snippetType = "autosnippet",
	}, {
		t({"\\Leftrightarrow"}),
	}, {
		condition = is_math
	}),

	s({
		trig = "set",
		name = "Set",
		snippetType = "autosnippet",
	}, {
		t({"\\{"}),
		i(1),
		t({"\\}"})
	}, {
		condition = is_math
	}),

	s({
		trig = "tpl",
		name = "Tupel",
		snippetType = "autosnippet",
	}, {
		t({"("}),
		i(1),
		t({")"})
	}, {
		condition = is_math
	}),

	s({
		trig = "range",
		name = "Create range of numbers",
		snippetType = "autosnippet",
	}, {
		i(1, "1"),
		t({", \\dots, "}),
		i(0, "n")
	}, {
		condition = is_math
	}),

	s({
		trig = "irange",
		name = "Create range of indexes",
		snippetType = "autosnippet",
	}, {
		i(1, "x"),
		t({"_{"}),
		i(2, "1"),
		t({"}, \\dots, "}),
		f(function(args, s, f) return args[1][1] end, {1}),
		t({"_{"}),
		i(3, "n"),
		t({"}"}),
	}, {
		condition = is_math
	}),

	s({
		trig = "srange",
		name = "Smart range",
		snippetType = "autosnippet",
	}, {
		i(1, "x"),
		t({", \\dots, "}),
		d(2, smart_range, {1}),
	}, {
		condition = is_math
	}),

	s({
		trig = "min",
		name = "Minimum",
		snippetType = "autosnippet",
	}, {
		t({"\\min\\{"}),
		i(1),
		t({"\\}"}),
	}, {
		condition = is_math
	}),

	s({
		trig = "max",
		name = "Maximum",
		snippetType = "autosnippet",
	}, {
		t({"\\max\\{"}),
		i(1),
		t({"\\}"}),
	}, {
		condition = is_math
	}),

	s({
		trig = "sum",
		name = "Sum",
		snippetType = "autosnippet",
	}, {
		t({"\\sum_{"}),
		i(1, "i = 0"),
		t({"}^{"}),
		i(2, "\\infty"),
		t({"} "}),
	}, {
		condition = is_math
	}),

	s({
		trig = "prod",
		name = "Product",
		snippetType = "autosnippet",
	}, {
		t({"\\prod_{"}),
		i(1, "i = 0"),
		t({"}^{"}),
		i(2, "\\infty"),
		t({"} "}),
	}, {
		condition = is_math
	}),

	s({
		trig = "nl",
		name = "New line",
		snippetType = "autosnippet",
	}, {
		t({"\\\\", ""}),
	}, {
		condition = is_math
	}),

	s({
		trig = "bb",
		name = "mathbb",
		snippetType = "autosnippet",
	}, {
		t({"\\mathbb{"}),
		i(1, "N"),
		t({"} "})
	}, {
		condition = is_math
	}),

	s({
		trig = "equiv",
		name = "Equivalence",
		snippetType = "autosnippet",
	}, {
		t({"\\equiv_{"}),
		i(1, "m"),
		t({"} "})
	}, {
		condition = is_math
	}),

	s({
		trig = "neq",
		name = "Not equal",
		snippetType = "autosnippet",
	}, {
		t({"\\neq "}),
	}, {
		condition = is_math
	}),

	s({
		trig = "over",
		name = "Overset",
		snippetType = "autosnippet",
	}, {
		t({"\\overset{"}),
		i(1),
		t({"}{"}),
		i(2),
		t({"} "}),
	}, {
		condition = is_math
	}),
}

