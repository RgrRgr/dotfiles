return {
  "lewis6991/gitsigns.nvim",
  dependencies = "seanbreckenridge/gitsigns-yadm.nvim",
  event = {"BufReadPre", "BufNewFile"},
  keys = {
    {"<leader>gb", "<cmd>Gitsigns toggle_current_line_blame<cr>", desc = "Toggle current line blame"},
    {"<leader>gs", "<cmd>Gitsigns stage_hunk<cr>", desc = "Stage hunk at point"},
    {"<leader>gS", "<cmd>Gitsigns stage_buffer<cr>", desc = "Stage file"},
    {"<leader>gU", "<cmd>Gitsigns undo_stage_hunk<cr>", desc = "Unstage hunk"},
    {"<leader>gR", "<cmd>Gitsigns reset_buffer<cr>", desc = "Reset buffer"},
    {"<leader>gr", "<cmd>Gitsigns reset_hunk<cr>", desc = "Reset hunk at point"}
  },
  opts = {
    _on_attach_pre = function(_, callback)
      require("gitsigns-yadm").yadm_signs(callback)
    end,
  }
}
