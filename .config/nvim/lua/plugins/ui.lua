return {
	{
		"stevearc/dressing.nvim",
		config = true,
		event = "VeryLazy",
		-- TODO use fzf backend?
	},
	-- TODO Replace with different statusline?
	{
		"nvim-lualine/lualine.nvim",
		dependencies = "nvim-lualine/lualine.nvim",
		config = true
	},
	{
		"folke/noice.nvim",
		dependencies = {
			"MunifTanjim/nui.nvim",
			"rcarriga/nvim-notify"
		},
		opts = {
			lsp = {
				override = {
					["vim.lsp.util.convert_input_to_markdown_lines"] = true,
					["vim.lsp.util.stylize_markdown"] = true,
					["cmp.entry.get_documentation"] = true
				}
			},
			cmdline = {view = "cmdline"},
			presets = {bottom_search = true, command_palette = true, inc_rename = false},
			messages = {enabled = false}
		},
		enabled = false
	}
}
