return {
	{
		"hrsh7th/nvim-cmp",
		event = { "InsertEnter", "CmdlineEnter" },
		dependencies = {
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-cmdline",
			"hrsh7th/cmp-path",
			"onsails/lspkind-nvim",
			"nvim-tree/nvim-web-devicons",
			"hrsh7th/cmp-nvim-lsp-signature-help",
			-- Let luasnip load first
			"LuaSnip",
			"nvim-autopairs",
		},
		opts = function()
			local cmp = require("cmp")
			local lspkind = require("lspkind")
			return {
				completion = {
					autocompletion = {
						cmp.TriggerEvent.TextChanged,
						cmp.TriggerEvent.InsertEnter
					},
					keyword_pattern = "\\k\\+"
				},
				view = { entries = { name = "custom" } },
				sources = cmp.config.sources(
					{ { name = "nvim_lsp" }, { name = "nvim_lsp_signature_help" }, { name = "path" } },
					{ { name = "buffer", option = { keyword_pattern = "\\k\\+" } } }
				),
				-- window = {
				-- 	completion = cmp.config.window.bordered({
				-- 		winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:Visual,Search:None"
				-- 	}),
				-- 	documentation = cmp.config.window.bordered({
				-- 		winhighlight = "Normal:Normal,FloatBorder:FloatBorder,CursorLine:Visual,Search:None"
				-- 	})
				-- },
				formatting = {
					format = lspkind.cmp_format({
						mode = "symbol",
						maxwidth = 50,
						ellipsis_char = "...",
						before = function(entry, vim_item)
							return vim_item
						end
					})
				},
				mapping = {
					["<C-Space>"] = cmp.mapping.complete(),
					["<C-e>"] = cmp.mapping.abort(),
					["<CR>"] = cmp.mapping.confirm({ select = false }),
					["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(4)),
					["<C-u>"] = cmp.mapping(cmp.mapping.scroll_docs(-4)),
					-- ["<Tab>"] = cmp.mapping(_3_, { "i", "c" }),
					-- ["<S-Tab>"] = cmp.mapping(_5_, { "i", "c" }),
					["<C-j>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							return cmp.select_next_item()
						else
							return fallback()
						end
					end, { "i", "c" }),
					-- ["<C-k>"] = cmp.mapping(_9_, { "i", "c" })
					["<C-k>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							return cmp.select_prev_item()
						else
							return fallback()
						end
					end, { "i", "c" }),
				}
			}
		end,
		config = function(_, opts)
			-- Source priority is handled via group index
			-- Do this to fix luasnip not having it set
			for _, source in ipairs(opts.sources) do
				source.group_index = source.group_index or 1
			end

			local cmp = require("cmp")
			cmp.setup(opts)
			cmp.setup.cmdline(":",
				---@diagnostic disable-next-line: missing-fields
				{
					mapping = cmp.mapping.preset.cmdline(),
					sources = cmp.config.sources(
						{ { name = "path" } },
						{ { name = "cmdline" } }
					)
				})
			cmp.setup.cmdline(
				{ "/", "?" },
				---@diagnostic disable-next-line: missing-fields
				{
					mapping = cmp.mapping.preset.cmdline(),
					sources = cmp.config.sources({ { name = "buffer" } })
				})
			-- local cmp_autopairs = require('nvim-autopairs.completion.cmp')
			-- cmp.event:on(
			-- 	'confirm_done',
			-- 	cmp_autopairs.on_confirm_done()
			-- )
		end,
	},
	{
		"L3MON4D3/LuaSnip",
		opts = {
			enable_autosnippets = true
		},
		config = function(_, opts)
			local luasnip = require("luasnip")
			local loaders = require("luasnip.loaders.from_lua")
			luasnip.setup(opts)
			loaders.load({ paths = "./lua/snippets" })
		end,
		dependencies = {
			"nvim-cmp",
			dependencies = { "saadparwaiz1/cmp_luasnip" },
			opts = function(_, opts)
				opts.snippet = {
					expand = function(args)
						require("luasnip").lsp_expand(args.body)
					end
				}
				table.insert(opts.sources, { name = "luasnip" })
			end
		},
		keys = {
			{
				"<tab>",
				function()
					local ls = require("luasnip")
					if ls.expand_or_jumpable() then
						-- Lua function errors out
						-- Lazy shenanigans, works with vim.keymap.set
						return "<Plug>luasnip-expand-or-jump"
					else
						return "<tab>"
					end
				end,
				expr = true,
				silent = true,
				mode = "i",
			},
			{ "<tab>",   function() require("luasnip").jump(1) end,  mode = "s" },
			{ "<s-tab>", function() require("luasnip").jump(-1) end, mode = { "i", "s" } },
			{
				"<C-j>",
				function()
					local ls = require("luasnip")
					if ls.choice_active() then
						ls.change_choice(-1)
					end
				end,
				mode = { "i", "s" }
			},
			{
				"<C-k>",
				function()
					local ls = require("luasnip")
					if ls.choice_active() then
						ls.change_choice(1)
					end
				end,
				mode = { "i", "s" }
			},
		},
	},
	{ "windwp/nvim-autopairs", event = "InsertEnter", opts = {} },
	{
		"kylechui/nvim-surround",
		keys = {
			{ "gs",  mode = { "n", "x" } },
			{ "dgs", mode = "n" },
			{ "cgs", mode = "n" },
		},
		opts = {
			keymaps = {
				normal = "gs",
				normal_cur = "gss",
				normal_line = "gS",
				normal_cur_line = "gSS",
				visual = "gs",
				visual_line = "gS",
				delete = "dgs",
				change = "cgs",
				change_line = "cgS",
			}
		}
	},
	{
		"chrisgrieser/nvim-various-textobjs",
		event = "VeryLazy",
		opts = {
			useDefaultKeymaps = true,
			disabledKeymaps = { "gc", "r" },
		},
	}
}
