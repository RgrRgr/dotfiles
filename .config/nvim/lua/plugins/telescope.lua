local function _2_()
	local ts = require("telescope.builtin")
	return ts.find_files({ find_command = { "fd", "-t", "d" } })
end
local function _3_()
	local ts = require("telescope.builtin")
	return ts.find_files({ cwd = vim.fn.stdpath("config") })
end

return {
	"nvim-telescope/telescope.nvim",
	dependencies = { "nvim-lua/plenary.nvim" },
	cmd = "Telescope",
	opts = function()
		local action = require("telescope.actions")
		return {
			defaults = {
				mappings = {
					i = {
						["<Esc>"] = action.close,
						["<C-j>"] = action.move_selection_next,
						["<C-k>"] = action.move_selection_previous
					}
				}
			}
		}
	end,
	keys = {
		{ "<leader>ff", "<cmd>Telescope find_files<cr>",  desc = "Find files" },
		{ "<leader>fg", "<cmd>Telescope live_grep<cr>",   desc = "Live grep files" },
		{ "<leader>fG", "<cmd>Telescope live_grep<cr>",   desc = "Grep word at cursor" },
		{ "<leader>fr", "<cmd>Telescope oldfiles<cr>",    desc = "Find recent files" },
		{ "<leader>fd", _2_,                              desc = "Find directories" },
		{ "<leader>fp", _3_,                              desc = "Find file in private config" },
		{ "<leader>Pf", "<cmd>Telescope git_files<cr>",   desc = "Find file in project" },
		{ "<leader>bb", "<cmd>Telescope buffers<cr>",     desc = "Switch buffer" },
		{ "<leader>sm", "<cmd>Telescope marks<cr>",       desc = "Jump to mark" },
		{ "<leader>sj", "<cmd>Telescope jumplist<cr>",    desc = "Jumplist" },
		{ "<leader>ir", "<cmd>Telescope registers<cr>",   desc = "From register" },
		{ "<leader>ht", "<cmd>Telescope colorscheme<cr>", desc = "Select colorscheme" },
		{ "<leader>cq", "<cmd>Telescope quickfix<cr>",    desc = "Open quickfix" },
		{ "<leader>cx", "<cmd>Telescope diagnostics<cr>", desc = "Show diagnostics" },
	}
}
