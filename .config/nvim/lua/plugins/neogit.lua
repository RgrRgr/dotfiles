local function _1_()
  local git = require("neogit")
  return git.open({cwd = (vim.fn.getenv("HOME") .. "/.local/share/yadm/repo.git")})
end
return {
  "NeogitOrg/neogit",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-telescope/telescope.nvim",
    "sindrets/diffview.nvim"
  },
  opts = {
    disable_insert_on_commit = "auto"
  },
  cmd = "Neogit",
  keys = {
    {"<leader>gg", "<cmd>Neogit<cr>", desc = "Open Neogit"},
    {"<leader>gp", _1_, desc = "Open Neogit for yadm"}
  }
}
