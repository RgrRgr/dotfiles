return {
	"folke/which-key.nvim",
	event = "VeryLazy",
	opts = {
		key_labels = {["<space>"] = "SPC", ["<cr>"] = "RET", ["<tab>"] = "TAB"}
	},
	config = function(_, opts)
		local wk = require("which-key")
		wk.setup(opts)

		wk.register({
			["<leader>b"] = {name = "+buffer"},
			["<leader>c"] = {name = "+code"},
			["<leader>f"] = {name = "+file"},
			["<leader>g"] = {name = "+git"},
			["<leader>h"] = {name = "+help"},
			["<leader>i"] = {name = "+insert"},
			["<leader>n"] = {name = "+notes"},
			["<leader>nj"] = {name = "+journal"},
			["<leader>s"] = {name = "+search"},
			["<leader>t"] = {name = "+toggle"},
		})
	end
}
