return {
	-- Nice
	-- hat auch integrations
	{
		'Everblush/nvim',
		name = 'everblush',
		config = function()
			vim.cmd("colorscheme everblush")
		end,
	},
	-- auch ok
	{ "tiagovla/tokyodark.nvim" },
	-- bisschen blass
	{ "kvrohit/rasmus.nvim" },
	-- noch blasser
	{ "mellow-theme/mellow.nvim" },
	-- nice
	{ "dasupradyumna/midnight.nvim" },
	-- wieder blass aber mit Grünstich
	{ "CrispyBaccoon/evergarden" },
	-- Sehr schwarz/blau
	{ "rockerBOO/boo-colorscheme-nvim" },
	{ "rebelot/kanagawa.nvim" },
	{
		"Shatur/neovim-ayu",
		priority = 1000,
		opts = { mirage = false },
		config = function(_, opts)
			local ayu = require("ayu")
			ayu.setup(opts)
			ayu.colorscheme()
		end,
		main = "ayu",
		enabled = false,
	},
	{
		"EdenEast/nightfox.nvim",
		opts = { options = { styles = { comments = "italic", dim_inactive = true } } },
		config = function(_, opts)
			local nightfox = require("nightfox")
			nightfox.setup(opts)
			vim.cmd("colorscheme carbonfox")
		end,
		enabled = false
	},
	{
		"marko-cerovac/material.nvim",
		opts = {
			plugins = {
				"gitsigns",
				"hop",
				"indent-blankline",
				"neogit",
				"nvim-cmp",
				"nvim-web-devicons",
				"telescope",
				"trouble",
				"which-key"
			}
		}
	},
	{ "folke/tokyonight.nvim" }
}
