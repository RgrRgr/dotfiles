local function _1_(_, _0)
	local align = require("mini.align")
	return align.setup({ mappings = { start = "", start_with_preview = "<leader>cF" } })
end
return {
	{
		"echasnovski/mini.align",
		opts = {
			mappings = {
				start = "",
				start_with_preview = "<leader>cF"
			}
		},
		keys = {
			{ "<leader>cF", nil, desc = "Align region" }
		},
	},
	{
		"echasnovski/mini.comment",
		config = true,
		keys = {
			{ "gcc", mode = "n" },
			{ "gc",  mode = { "v", "n" } },
		},
	},
	{
		"echasnovski/mini.cursorword",
		config = true,
		event = "VeryLazy"
	},
	{
		"echasnovski/mini.jump",
		config = true,
		event = "VeryLazy"
	} }
