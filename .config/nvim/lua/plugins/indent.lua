return {
	enabled = false,
	"lukas-reineke/indent-blankline.nvim",
	commit = "4541d690816cb99a7fc248f1486aa87f3abce91c",
	opts = {
		show_current_context = true,
		show_current_context_start = true,
		char_highlight_list = {
			"IndentBlanklineIndent1",
			"IndentBlanklineIndent2",
			"IndentBlanklineIndent3",
			"IndentBlanklineIndent4",
			"IndentBlanklineIndent5",
			"IndentBlanklineIndent6"
		}
	}
}
