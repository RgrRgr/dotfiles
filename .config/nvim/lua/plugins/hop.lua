return {
	"phaazon/hop.nvim",
	opts = {},
	keys = {
		{ "gw", "<cmd>HopWord<cr>",    mode = { "n", "v", "o" }, desc = "Hop to word" },
		{ "gl", "<cmd>HopLine<cr>",    mode = { "n", "v", "o" }, desc = "Hop to line" },
		{ "g/", "<cmd>HopPattern<cr>", mode = { "n", "v", "o" }, desc = "Hop pattern" },
		{
			"s",
			function()
				local direction = require("hop.hint").HintDirection
				require("hop").hint_char2({ direction = direction.AFTER_CURSOR })
			end,
			mode = { "n", "v", "o" }
		},
		{
			"S",
			function()
				local direction = require("hop.hint").HintDirection
				require("hop").hint_char2({ direction = direction.BEFORE_CURSOR })
			end,
			mode = { "n", "v", "o" }
		}
	}
}
