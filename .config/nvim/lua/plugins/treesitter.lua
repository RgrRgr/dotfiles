return {
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		-- event = {"BufReadPost", "BufNewFile"},
		-- Can't be lazy loaded apparently maybe
		event = { "VeryLazy" },
		cmd = { "TSUpdateSync", "TSUpdate", "TSInstall" },
		dependencies = { "nvim-treesitter/nvim-treesitter-textobjects" },
		opts = {
			highlight = {
				enable = true,
				additional_vim_regex_highlighting = false,
			},
			indent = { enable = true },
			sync_install = false,
			auto_install = true,
			textobjects = {
				select = {
					enable = true,
					keymaps = {
						["af"] = { query = "@function.outer", desc = "Select outer part of function region" },
						["if"] = { query = "@function.inner", desc = "Select inner part of function region" },
						["ac"] = { query = "@class.outer", desc = "Select outer part of class region" },
						["ic"] = { query = "@class.inner", desc = "Select inner part of class region" },
					}
				},
				move = {
					enable = true,
					goto_next_start = { ["]f"] = "@function.outer", ["]c"] = "@class.outer" },
					goto_next_end = { ["]F"] = "@function.outer", ["]C"] = "@class.outer" },
					goto_previous_start = { ["[f"] = "@function.outer", ["[c"] = "@class.outer" },
					goto_previous_end = { ["[F"] = "@function.outer", ["[C"] = "@class.outer" },
				},
			},
			ensure_installed = {
				"bash",
				"diff",
				"git_config",
				"git_rebase",
				"gitattributes",
				"gitcommit",
				"gitignore",
				"lua",
				"luadoc",
				"luap",
				"markdown",
				"markdown_inline",
				"python",
			},
		},
		config = function(_, opts)
			require("nvim-treesitter.configs").setup(opts)
			-- Use treesitter for folding
			vim.opt.foldmethod = "expr"
			vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
			-- Disable folding at startup
			vim.opt.foldenable = false
		end
	},
	{
		"nvim-treesitter/nvim-treesitter-context",
		-- INFO: BufWritePost for nvim -> :w file.lua -> load lua
		event = { "BufNewFile", "BufReadPost", "BufWritePost" },
		opts = {
			mode = "cursor",
			max_lines = 3,
			-- multiline_threshold = 1,
		},
		keys = {
			{ "<leader>tc", "<cmd>TSContextToggle<cr>", desc = "Toggle Treesitter context" },
			{
				"[t",
				function()
					require("treesitter-context").go_to_context(vim.v.count1)
				end,
				desc = "Goto beginning of context"
			}
		}
	}
}
