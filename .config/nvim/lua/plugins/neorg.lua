local function _1_()
  local ts = require("telescope.builtin")
  return ts.find_files({cwd = "~/Documents/neorg/"})
end
local function _2_()
  local ts = require("telescope.builtin")
  return ts.find_files({cwd = "~/Documents/neorg/journal/"})
end

return {
	"nvim-neorg/neorg",
	build = ":Neorg sync-parsers",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-telescope/telescope.nvim"
	},
	ft = "norg",
	keys = {
		{"<leader>ni", "<cmd>Neorg inject-metadata<cr>", desc = "Create metadata"},
		{"<leader>nu", "<cmd>Neorg update-metadata<cr>", desc = "Update metadata"},
		{"<leader>nf", _1_, desc = "Find notes"},
		{"<leader>njf", _2_, desc = "Find journal"},
		{"<leader>njj", "<cmd>Neorg journal today<cr>", desc = "Open journal for today"},
		{"<leader>njt", "<cmd>Neorg journal tomorrow<cr>", desc = "Open journal for tomorrow"},
		{"<leader>njT", "<cmd>Neorg journal template<cr>", desc = "Open journal template"},
		{"<leader>njy", "<cmd>Neorg journal yesterday<cr>", desc = "Open journal for yesterday"},
		{"<leader>njo", "<cmd>Neorg toc open<cr>", desc = "Open table of contents"},
		{"<leader>nju", "<cmd>Neorg toc update<cr>", desc = "Update table of contents"}
	},
	config = function()
		require("neorg").setup({
			load = {
				["core.defaults"] = {},
				["core.concealer"] = {},
				["core.completion"] = {config = {engine = "nvim-cmp"}},
				["core.journal"] = {config = {workspace = "normal"}},
				["core.dirman"] = {config = {workspaces = {normal = "~/Documents/neorg"}}}}
		})
	end
}
