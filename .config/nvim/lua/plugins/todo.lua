return {
	"folke/todo-comments.nvim",
	dependencies = { "nvim-lua/plenary.nvim" },
	cmd = { "TodoTrouble", "TodoTelescope" },
	event = {"BufNewFile", "BufReadPost", "BufWritePost"},
	opts = {
		signs = false,
		highlight = {
			keyword = "bg"
		}
	},
	-- TODO: add "leader s" as search category,
	-- f for looking for files
	-- s for looking in files
	keys = {
		{ "]t",         function() require("todo-comments").jump_next() end, desc = "Next todo comment" },
		{ "[t",         function() require("todo-comments").jump_prev() end, desc = "Previous todo comment" },
		{ "<leader>ct", "<cmd>TodoTrouble<cr>",                              desc = "Todo (Trouble)" },
		{ "<leader>cT", "<cmd>TodoTrouble keywords=TODO,FIX,FIXME<cr>",      desc = "Todo/Fix/Fixme (Trouble)" },
		{ "<leader>ft", "<cmd>TodoTelescope<cr>",                            desc = "Todo" },
		{ "<leader>fT", "<cmd>TodoTelescope keywords=TODO,FIX,FIXME<cr>",    desc = "Todo/Fix/Fixme" },
	},
}
