local function toggle_lsp_lines()
	if vim.diagnostic.config().virtual_text then
		vim.diagnostic.config({
			virtual_text = false,
			virtual_lines = true
		})
	else
		vim.diagnostic.config({
			virtual_text = true,
			virtual_lines = false
		})
	end
end

return {
	{
		"neovim/nvim-lspconfig",
		event = {"BufReadPre", "BufNewFile"},
		dependencies = {
			{ "folke/neodev.nvim", config = true },
		},
		config = function(_, opts)
			local lspconfig = require("lspconfig")
			local cmp_lsp = require("cmp_nvim_lsp")
			local capabilities = vim.tbl_deep_extend("force",
				{},
				vim.lsp.protocol.make_client_capabilities(),
				cmp_lsp.default_capabilities()
			)

			for lspname, config in pairs(opts.servers) do
				local lsp = lspconfig[lspname]
				lsp.setup({
					capabilities = capabilities,
					settings = config
				})
			end
			-- This doesn't really do anything on 0.9
			vim.diagnostic.config(vim.deepcopy(opts.diagnostic))

			-- TODO remove when upgrading to 0.10
			local signs = { Error = "󰅚 ", Warn = "󰀪 ", Hint = "󰌶 ", Info = " " }
			for type, icon in pairs(signs) do
				local hl = "DiagnosticSign" .. type
				vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
			end
		end,
		opts = {
			diagnostic = {
				severity_sort = true,
				signs = {
					text = {
						[vim.diagnostic.severity.ERROR] = "󰅚 ",
						[vim.diagnostic.severity.WARN] = "󰀪 ",
						[vim.diagnostic.severity.HINT] = "󰌶 ",
						[vim.diagnostic.severity.INFO] = " ",
					},
				},
			},
			servers = {
				rust_analyzer = {
					["rust-analyzer"] = {
						checkOnSave = {
							overrideCommand = {"cargo", "clippy", "--workspace", "--message-format=json", "--all-targets", "--all-features"}
						}
					},
				},
				texlab = {
					texlab = {
						build = {
							onSave = true
						},
						forwardSearch = {
							executable = "zathura", args = {"--synctex-forward", "%l:1%f", "%p"}
						}
					},
				},
				lua_ls = {
					lua_ls = {
						settings = {
							Lua = {
								completion = {
									callSnippet = "Replace"
								}
							}
						}
					}
				}
			},
		}
	},
	{
		"https://git.sr.ht/~whynothugo/lsp_lines.nvim",
		config = function()
			require("lsp_lines").setup()
			vim.diagnostic.config({
				virtual_lines = not vim.diagnostic.config().virtual_text
			})
		end,
		keys = {
			{"<leader>tl", toggle_lsp_lines, desc = "Toggle lsp lines"},
			{"<leader>cl", toggle_lsp_lines, desc = "Toggle lsp lines"}
		}
	},
	{
		"folke/trouble.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = true,
		keys = {
			{"<leader>cX", "<cmd>Trouble workspace_diagnostics<cr>", desc = "Display workspace diagnostics"},
			{"<leader>cq", "<cmd>Trouble quickfix<cr>", desc = "Display quickfix"},
			{"<leader>cc", "<cmd>Trouble loclist<cr>", desc = "Display loclist"},
		}
	}
}
