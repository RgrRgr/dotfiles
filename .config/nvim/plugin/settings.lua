-- See `:help vim.o`
-- Global variables
vim.g.tex_conceal = "abdmgs"
vim.g.mapleader = " "

local opt = vim.opt
-- (Neo)Vim settings
opt.mouse = "" -- Real men don't use a mouse
opt.completeopt = "menu,menuone,noselect,preview,noinsert" -- configure completion
opt.confirm = true -- Ask to save changes
opt.grepprg = "rg --vimgrep" -- use rg instead of grep

-- UI settings
opt.scrolloff = 10                  -- Add some scrolloff for comfort
opt.sidescrolloff = 10              -- Add some scrolloff for comfort
opt.splitbelow = true               -- open new vertical split bottom
opt.splitright = true               -- open new horizontal splits right
opt.termguicolors = true            -- We've reached the 21st century
opt.number = true                   -- Where are we?
opt.relativenumber = true           -- we're cool like that
opt.cursorline = true               -- Makes spotting the cursor easier
opt.list = true                     -- display special characters
opt.listchars = { trail = "+", tab = "  " }     -- Show trailing spaces
opt.conceallevel = 2                -- Hide markup symbols
opt.laststatus = 2                  -- Controls the status line
opt.showmode = false                -- not needed since I have a proper status line :)
opt.signcolumn = "yes:2" -- Always enable signcolumn with space for two signs
opt.numberwidth = 3 -- 3 digits should usually be enough
-- INFO: adds annoying PRESS ENTER TO CONFIRM messages :(
-- opt.cmdheight = 0 -- Hide cmdline

-- Experimental settings
opt.pumblend = 10 -- Popup blend
opt.pumheight = 10 -- Maximum number of entries in a popup
opt.splitkeep = "screen" -- what does this even do?
opt.virtualedit = "block"

-- Searching settings
opt.ignorecase = true           -- ignore case in searches by default
opt.smartcase = true            -- but make it case sensitive if an uppercase is entered

-- Indentation
opt.tabstop = 4 -- Size of a single tab
opt.shiftwidth = 4 -- How much to indent, should be same as tabstop
opt.copyindent = true
-- expandtab
-- shiftround
-- smartindent
-- smarttab
-- softtabstop
-- preserveindent
-- indentexpr
-- breakindent
--
-- Folding
-- opt.foldlevelstart = 99
-- foldtext

-- opt("w", "foldmethod", "expr")
-- opt("w", "foldexpr", "v:lua.vim.treesitter.foldexpr()")

if vim.fn.has("nvim-0.10") == 1 then
  opt.smoothscroll = true
end

--
-- opt("g", "timeoutlen", 500)
-- opt("g", "ttymouse", "")
-- opt("b", "tabstop", 4)
-- opt("o", "hidden", true)
-- opt("w", "linebreak", true)
-- opt("w", "signcolumn", "yes:2")
-- opt("w", "wrap", true)
-- opt("w", "cursorcolumn", false)
-- opt("o", "syntax", "enable")
-- opt("o", "guicursor", "a:block")

do
  local lsp = vim.lsp
  lsp.handlers["textDocument/hover"] = lsp.with(vim.lsp.handlers.hover, {border = "rounded"})
end
local ns = vim.api.nvim_create_namespace("diagnostic_signs")
local orig_signs_handler = vim.diagnostic.handlers.signs
local function _1_(_, bufnr, _0, opts)
  local diagnostics = vim.diagnostic.get(bufnr)
  local max_severity_per_line = {}
  for _1, d in pairs(diagnostics) do
    local m = max_severity_per_line[d.lnum]
    if (not m or (d.severity < m.severity)) then
      max_severity_per_line[d.lnum] = d
    else
    end
  end
  return orig_signs_handler.show(ns, bufnr, vim.tbl_values(max_severity_per_line), opts)
end
local function _3_(_, bufnr)
  return orig_signs_handler.hide(ns, bufnr)
end
vim.diagnostic.handlers["signs"] = {show = _1_, hide = _3_}
return nil
