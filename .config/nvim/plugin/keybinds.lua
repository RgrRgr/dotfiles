local set = vim.keymap.set
local k = vim.keycode

local remap = {noremap = false}
set("n", "<leader>x", "<cmd>.lua<CR>", { desc = "Execute the current line" })
set("v", "<leader>x", "<cmd>'<,'>source %<CR>", { desc = "Execute selected region" })
set("n", "<leader><leader>x", "<cmd>source %<CR>", { desc = "Execute the current file" })

set("n", "U", "<C-r>", remap)
set("", "<leader>y", "\"+y", {desc = "Yank to clipboard"})
set("", "<leader>p", "\"+p", {desc = "Paste from clipboard"})

set("n", "<leader>bp", "<cmd>bprevious<cr>", {desc = "Switch to previous buffer"})
set("n", "<leader>bk", "<cmd>bdelete<cr>", {desc = "Close current buffer"})
set("n", "<leader>bn", "<cmd>bnext<cr>", {desc = "Close current buffer"})

set("n", "n", "nzzzv")
set("n", "N", "Nzzzv")

-- Keymaps for better default experience
-- See `:help set()`
-- apparently this is useful
set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- better up/down
set({ "n", "x" }, "j",      "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
set({ "n", "x" }, "<Down>", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
set({ "n", "x" }, "k",      "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
set({ "n", "x" }, "<Up>",   "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })

-- Move to window using the <ctrl> hjkl keys
set("n", "<c-j>", "<c-w><c-j>")
set("n", "<c-k>", "<c-w><c-k>")
set("n", "<c-l>", "<c-w><c-l>")
set("n", "<c-h>", "<c-w><c-h>")

-- Resize window using <ctrl> arrow keys
set("n", "<C-Up>", "<cmd>resize +2<cr>", { desc = "Increase window height" })
set("n", "<C-Down>", "<cmd>resize -2<cr>", { desc = "Decrease window height" })
set("n", "<C-Left>", "<cmd>vertical resize -2<cr>", { desc = "Decrease window width" })
set("n", "<C-Right>", "<cmd>vertical resize +2<cr>", { desc = "Increase window width" })

-- Clear search with <esc>
set("n", "<esc>", "<cmd>noh<cr><esc>", { desc = "Escape and clear hlsearch" })

-- https://github.com/mhinz/vim-galore#saner-behavior-of-n-and-n
-- set("n", "n", "'Nn'[v:searchforward].'zv'", { expr = true, desc = "Next search result" })
-- set("x", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
-- set("o", "n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
-- set("n", "N", "'nN'[v:searchforward].'zv'", { expr = true, desc = "Prev search result" })
-- set("x", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
-- set("o", "N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })

-- Add undo break-points
set("i", ",", ",<c-g>u")
set("i", ".", ".<c-g>u")
set("i", ";", ";<c-g>u")

-- Better indenting
set("v", "<", "<gv")
set("v", ">", ">gv")

-- Diagnostic keymaps
set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
-- set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
-- set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })

-- LSP bindings
-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    -- vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    set('n', 'gD', vim.lsp.buf.declaration, { buffer = ev.buf, desc = "Go Declaration"})
    set('n', 'gd', vim.lsp.buf.definition, { buffer = ev.buf, desc = "Go Definition"})
    set('n', 'K', vim.lsp.buf.hover, { buffer = ev.buf, desc = "Open Hover"})
    set('n', 'gi', vim.lsp.buf.implementation, { buffer = ev.buf, desc = "Go Implementation"})
    set('n', '<C-k>', vim.lsp.buf.signature_help, { buffer = ev.buf, desc = "Signature help"})
    set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, { buffer = ev.buf, desc = "Add workspace folder"})
    set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, { buffer = ev.buf, desc = "Remove workspace folder"})
    set('n', '<space>wl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, { buffer = ev.buf, desc = "Show workspace folders"})
    set('n', 'gt', vim.lsp.buf.type_definition, { buffer = ev.buf, desc = "Go Type Definition"})
    set('n', '<space>cr', vim.lsp.buf.rename, { buffer = ev.buf, desc = "Rename"})
    set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, { buffer = ev.buf, desc = "Code action"})
    set('n', 'gr', vim.lsp.buf.references, { buffer = ev.buf, desc = "Go References"})
    set('n', '<space>cf', function()
      vim.lsp.buf.format { async = true }
    end, { buffer = ev.buf, desc = "Format buffer"})
  end,
})

-- https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- hat noch viele nette keybinds, nochmal anschauen
