if status is-interactive
    # Commands to run in interactive sessions can go here
    starship init fish | source
    zoxide init fish | source

    alias cp="cp -ir"
    alias mv="mv -i"
    alias mkdir="mkdir -p"
    alias rg="rg --smart-case"

	#fish_ssh_agent
end

function nve
	fd ebuild /var/db/repos | fzf --multi | xargs nvim
end
#direnv hook fish | source
#vim: ft=bash
