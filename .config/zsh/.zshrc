# Programs that want to get sourced
# eval $(opam env)
# eval "$(starship init zsh)"
eval "$(zoxide init zsh)"

function nve() {
	fd ebuild /var/db/repos/ | fzf --multi | xargs nvim
}

function snve() {
	fd ebuild /var/db/repos/ | fzf --multi | xargs sudoedit
}

#
# Keybindings
# use cat -v to figure out
# Makes ctrl+left-arrow/right-arrow work
set -o emacs

bindkey '^[[1;5C' forward-word
bindkey '^[[1;5D' backward-word

# Shift tab for previous entry
bindkey '^[[Z' reverse-menu-complete

export LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:';

# Aliases
alias ls="eza"
alias rg="rg --smart-case"
# Basic functionality
autoload -Uz compinit; compinit -d "$XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION"
autoload -U promptinit; promptinit
prompt gentoo

# Cache results for faster access
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$XDG_CACHE_HOME/zsh/zcompcache"

zstyle ':completion:*' menu select
zstyle ':completion:*' complete-options true
zstyle ':completion:*' matcher-list '' \
    'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'  # partial completion

# #zstyle ':completion::complete:*' gain-privileges 1

#
# Ztyle pattern
# :completion:<function>:<completer>:<command>:<argument>:<tag> style values
# man zshcompsys
# Styles: /standard styles
# Tags: /standard tags
# more info: /zstyle
# color: /Colored completion listings
#

zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'
zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'

zstyle ':completion:*' squeeze-slashes true # Treat // as /
zstyle ':completion:*' group-name ''        # Group results

zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
#zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)*==02=01}:${(s.:.)LS_COLORS}")'
#zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)*==34=34}:${(s.:.)LS_COLORS}")';
#zstyle ':completion:*:descriptions' format '%BCompleting %d:%b'
#zstyle ':completion:*:parameters' list-colors '=(#b)*(-- *)=32=31' '=*=32'
zstyle ':completion:*:options' list-colors '=(#b)*(-- *)=32=31' '=*=33'

zstyle ':completion:*:commands' list-colors '=*=1;31'

#setopt correctall               # Ask to correct mistyped commands
setopt hist_ignore_all_dups     # Do not record duplicate events
setopt hist_ignore_space        # Do not record commands starting with space
setopt hist_find_no_dups        # Do not find duplicate commands when searching
setopt autocd                   # Allows to omit cd in cd <path>
setopt SHARE_HISTORY            # History is shared between zsh instances
#setopt COMPLETE_IN_WORD         # Complete from both ends of a word
# setopt extendedglob

AUTOSUGGESTIONS=/usr/share/zsh/site-functions/zsh-autosuggestions.zsh
if [ -e $AUTOSUGGESTIONS ]; then
    . $AUTOSUGGESTIONS
    #WORDCHARS='*?_-.[]~=&;!#$%^(){}<>' # Removes / so forward word will only partially complete paths
    WORDCHARS=${WORDCHARS/\/}
    bindkey '^@' forward-word
fi


SYNTAXHIGHLIGHTING=/usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh
if [ -e $SYNTAXHIGHLIGHTING ]; then
    . $SYNTAXHIGHLIGHTING
fi

SUBSTRINGSEARCH=/usr/share/zsh/site-functions/zsh-history-substring-search.zsh
if [ -e $SUBSTRINGSEARCH ]; then
    . $SUBSTRINGSEARCH
    bindkey '^[[A' history-substring-search-up
    bindkey '^[[B' history-substring-search-down
    HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND="bold"
fi

FZFPLUGIN=/usr/share/fzf/key-bindings.zsh
if [ -e $FZFPLUGIN ]; then
	. $FZFPLUGIN
fi

unset AUTOSUGGESTIONS
unset SYNTAXHIGHLIGHTING
unset SUBSTRINGSEARCH
unset FZFPLUGIN
