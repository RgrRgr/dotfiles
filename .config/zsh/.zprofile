### Paths ###
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_STATE_HOME=$HOME/.local/state
#export XDG_CACHE_HOME=$HOME/.cache
export XDG_CACHE_HOME=$HOME/.local/cache
export PATH="$HOME/.local/bin:$PATH"
#export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$(id -u)/bus"

### Clean home ###
#Rust
export CARGO_INSTALL_ROOT="$HOME/.local/"
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export CARGO_HOME="$XDG_DATA_HOME"/cargo

#Ocaml
export OPAMROOT="$XDG_DATA_HOME/opam"

#Go
export GOPATH="$XDG_DATA_HOME"/go
export GOMODCACHE="$XDG_CACHE_HOME"/go/mod

#Java
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java

#Js/npm
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc

#Other
export HISTSIZE=10000                               # Maximum events for internal history
export SAVEHIST=$HISTSIZE                           # Maximum events for history file
export HISTFILE="$XDG_STATE_HOME/zsh/history"       # History file location
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export W3M_DIR="$XDG_STATE_HOME/w3m"
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel

### Look and feel ###
export QT_QPA_PLATFORMTHEME=qt5ct
export GTK_THEME=Materia-dark
export BAT_THEME=base16
export XCURSOR_THEME=Adwaita
export XCURSOR_SIZE=24

### less ###
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# Other
export EDITOR=nvim
# export PAGER=nvimpager

if [ "$(tty)" = "/dev/tty1" ]; then
    exec Hyprland
fi
if [ "$(tty)" = "/dev/tty2" ]; then
    exec startx
fi
