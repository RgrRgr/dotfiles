;;; init.el --- Personal configuration file -*- lexical-binding: t; no-byte-compile: t; -*-

(defvar elpaca-installer-version 0.7)
(defvar elpaca-directory (expand-file-name "elpaca/" emacs-local-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
                              :ref nil
                              :files (:defaults "elpaca-test.el" (:exclude "extensions"))
                              :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
        (if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                 ((zerop (call-process "git" nil buffer t "clone"
                                       (plist-get order :repo) repo)))
                 ((zerop (call-process "git" nil buffer t "checkout"
                                       (or (plist-get order :ref) "--"))))
                 (emacs (concat invocation-directory invocation-name))
                 ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
                                       "--eval" "(byte-recompile-directory \".\" 0 'force)")))
                 ((require 'elpaca))
                 ((elpaca-generate-autoloads "elpaca" repo)))
            (progn (message "%s" (buffer-string)) (kill-buffer buffer))
          (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

;; Install use-package support
(elpaca elpaca-use-package
  ;; Enable :ensure use-package keyword.
  (elpaca-use-package-mode)
  ;; Assume :defer t unless otherwise specified.
  ;; (setq use-package-always-defer t)
  ;; Assume :ensure t unless otherwise specified.
  (setq use-package-always-ensure t))

;; Block until current queue processed.
(elpaca-wait)

(defmacro use-builtin (name &rest args)
  "Like `use-package' but accounting for asynchronous installation.
  NAME and ARGS are in `use-package'."
  (declare (indent defun))
  `(progn
     ;; (cl-pushnew (quote ,name) elpaca-ignored-dependencies)
     (use-package ,name
       :ensure nil
       ,@args)))
(elpaca-wait)

(defun display-startup-echo-area-message ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
		    (time-subtract (current-time) before-init-time)))
           gcs-done))
(when +benchmark
  (profiler-start 'cpu+mem)
  (add-hook 'elpaca-after-init-hook
	    (lambda () (profiler-stop) (profiler-report))
	    100)
  (setq use-package-verbose t
        use-package-expand-minimally nil
        use-package-compute-statistics t
        use-package-verbose nil
	use-package-expand-minimally t))

(when +benchmark
  (use-package benchmark-init
    :config
    (add-hook 'elpaca-after-init-hook #'benchmark-init/deactivate)))

(use-package kaolin-themes
  :config
  (load-theme 'kaolin-dark t))

(use-package doom-modeline
  :hook (elpaca-after-init . doom-modeline-mode)
  ;; :custom
  ;; (doom-modeline-buffer-file-name-style 'relative-to-project)
  ;; (doom-modeline-icon nil)
  :hook (doom-modeline . column-number-mode)
  :hook (doom-modeline . size-indication-mode))

(use-package general
  :config
  (general-evil-setup)
  (defun find-file-in-directory (directory)
    (lambda ()
      (interactive)
      (let ((default-directory directory))
  	(call-interactively 'find-file))))
  (general-define-key
   :keymaps 'override
   ;:states '(insert normal hybrid motion visual operator emacs)
   :states '(normal motion visual operator)
   :prefix-map '+prefix-map
   :prefix "SPC"
   :global-prefix "S-SPC")
  
  (general-create-definer global-definer
    :wk-full-keys nil
    :keymaps '+prefix-map)
  
  (defmacro +general-global-menu! (name prefix-key &rest body)
    "Create a definer named +general-global-NAME wrapping global-definer.
    Create prefix map: +general-global-NAME-map. Prefix bindings in BODY with PREFIX-KEY."
    (declare (indent 2))
    (let* ((n (concat "+general-global-" name))
           (prefix-map (intern (concat n "-map"))))
      `(progn
         (general-create-definer ,(intern n)
           :wrapping global-definer
           :prefix-map (quote ,prefix-map)
           :prefix ,prefix-key
           :wk-full-keys nil
           "" '(:ignore t :which-key ,name))
         (,(intern n) ,@body))))
  (+general-global-menu! "application" "a"
    "p" '(:ignore t "elpaca")
    "pb" 'elpaca-browse
    "pd" 'elpaca-delete
    "pf" 'elpaca-fetch
    "pF" 'elpaca-fetch-all
    "pr"  '((lambda () (interactive)
  	    (let ((current-prefix-arg (not current-prefix-arg))
  		  (this-command 'elpaca-rebuild))
  	      (call-interactively #'elpaca-rebuild)))
  	  :which-key "rebuild")
    "pm" 'elpaca-merge
    "pM" 'elpaca-merge-all
    "pp" 'elpaca-manager
    "pl" 'elpaca-log
    "pi" 'elpaca-info
    "pI" '((lambda () (interactive) (info "Elpaca"))
  	 :which-key "elpaca-info")
    "ps" 'elpaca-status
    "pt" 'elpaca-try
    "pv" 'elpaca-visit)
  (+general-global-menu! "file" "f"
    "r" #'recentf
    "p" (list (find-file-in-directory "~/.config/emacs/") :wk "Find in private config")
    "f" '(find-file :wk "Find file"))
  (+general-global-menu! "notes" "n")
  (+general-global-menu! "git" "g")
  (+general-global-menu! "buffer" "b"
    "b" '(switch-to-buffer :wk "Switch buffer")
    "k" '(kill-current-buffer :wk "Kill this buffer")
    "n" '(next-buffer :wk "Next buffer")
    "p" '(previous-buffer :wk "Previous buffer")
    "r" '(revert-buffer :wk "Reload buffer"))
  (+general-global-menu! "help" "h"
    "F" 'describe-face
    "'" 'describe-char
    "b" '(:ignore t :wk "Bindings")
    "b k" 'describe-keymap)
  (general-define-key
    "<XF86Paste>" 'clipboard-yank
    "<XF86Copy>" 'clipboard-kill-ring-save
    "<XF86Cut>" 'clipboard-kill-region
  
    "<Paste>" 'clipboard-yank
    "<Copy>" 'clipboard-kill-ring-save
    "<Cut>" 'clipboard-kill-region
    "C-0" 'text-scale-adjust
    "C--" 'text-scale-decrease
    "C-+" 'text-scale-increase)
  
  (global-definer
    ":" '(execute-extended-command :wk "M-x")
    ";" '(eval-expression :wk "Eval expression")
    "." '(find-file :wk "Find file"))
  
  (general-define-key
   :keymaps 'minibuffer-local-map
   "C-w" 'evil-delete-backward-word)
  
  (defvar-keymap +global-execute-keymap
    "r" #'eval-region
    "l" #'pp-eval-last-sexp
    "b" #'eval-buffer
    "d" #'eval-defun)
  (keymap-set +prefix-map "x" `("Execute" ,+global-execute-keymap))
  (general-create-definer global-leader
    :keymaps 'override
    :states '(normal motion visual operator)
    :prefix "SPC m"
    "" '( :ignore t
          :which-key
          (lambda (arg)
            (cons (cadr (split-string (car arg) " "))
                  (replace-regexp-in-string "-mode$" "" (symbol-name major-mode)))))))
(elpaca-wait)

(use-package evil
  :hook (elpaca-after-init . evil-mode)
  :init
  (setq evil-want-Y-yank-to-eol t
	evil-want-keybinding nil
	evil-vsplit-window-right t
	evil-split-window-below t
	evil-want-C-u-scroll t
	evil-want-C-u-delete t
	evil-search-wrap t
	evil-undo-system 'undo-redo
        ;; scheint nicht zu funktionieren
	evil-ex-search-count t)
  :config
  (evil-select-search-module 'evil-search-module 'evil-search)
  (advice-add 'evil-force-normal-state :after #'evil-ex-nohighlight)
  (general-nmap "U" #'undo-redo))

(use-package evil-collection
  :after evil
  :config
  ;(setq evil-collection-mode-list '(dashboard dired ibuffer elpaca))
  (evil-collection-init))
;; (use-package evil-tutor)
(setq annalist-record nil) ; Should make startup a little faster

(use-package evil-org
  :hook (org-mode . evil-org-mode)
  :config
  (evil-define-key '(normal visual) 'evil-org-mode
    "gl" nil))

(use-package evil-org-agenda
  :ensure nil
  :after org-agenda
  :config
  (evil-org-agenda-set-keys))

(use-package evil-surround
  :commands (evil-surround-edit
	     evil-Surround-edit
	     evil-surround-region
	     evil-Surround-region)
  :init
  (general-nmap
    "gs" 'evil-surround-edit
    "gS" 'evil-Surround-edit)
  (general-vmap
    "gs" 'evil-surround-region
    "gS" 'evil-Surround-region)
  (add-hook 'org-mode-hook
	    (lambda ()
	      (require 'evil-surround)
	      (dolist (symbols '((?= . ("=" . "="))
				 (?~ . ("~" . "~"))
				 (?/ . ("/" . "/"))
				 (?* . ("*" . "*"))
				 (?_ . ("_" . "_"))
				 (?+ . ("+" . "+"))))
		(push symbols evil-surround-pairs-alist))))
  :config
  (defun evil-surround-call-with-repeat (callback)
    (call-interactively callback)))

(use-package evil-nerd-commenter
  :commands (evilnc-comment-operator
             evilnc-inner-comment
             evilnc-outer-commenter)
  :init
  (general-def
    [remap comment-line] #'evilnc-comment-or-uncomment-lines)
  (general-nmap
   "gc" 'evilnc-comment-operator))

(use-package evil-lion
  :defer t
  :init
  (general-nmap
    "g l" 'evil-lion-left
    "g L" 'evil-lion-right))

(use-package evil-snipe
  :hook (on-first-input . evil-snipe-override-mode)
  ;; :hook (on-first-input . evil-snipe-mode)
  :config
  (setq evil-snipe-repeat-scope 'visible
	evil-snipe-char-fold t))

(use-package avy
  :commands (evil-avy-goto-char-2-above evil-avy-goto-char-2-below)
  :init
  (general-nmap
   "s" 'evil-avy-goto-char-2-below
   "S" 'evil-avy-goto-char-2-above)
  :config
  (setq avy-case-fold-search nil))

(use-package anzu
  :hook (on-first-input . global-anzu-mode))

(use-package evil-anzu
  :after anzu)

(use-package evil-numbers
  :defer t
  :init
  (general-nmap
   "C-a" #'evil-numbers/inc-at-pt
   "C-x" #'evil-numbers/dec-at-pt
   "g C-a" #'evil-numbers/inc-at-pt-incremental
   "g C-x" #'evil-numbers/dec-at-pt-incremental))

(global-auto-revert-mode)
(setq global-auto-revert-non-file-buffers t)

(use-builtin recentf
  :hook (on-first-file . recentf-mode)
  :commands recentf-open-files
  :config
  (setq recentf-max-saved-items 50
	recentf-auto-cleanup nil
	recentf-save-file (expand-file-name "recentf" emacs-cache-directory))
  (add-hook 'kill-emacs-hook #'recentf-cleanup))

(add-hook 'on-first-input-hook (lambda () (electric-pair-mode 1)))

(use-package ws-butler
  ;:hook (prog-mode . ws-butler-mode)
  :hook (on-first-buffer . ws-butler-global-mode))

(use-package on
  :ensure (on
	   :host github
	   :repo "ajgrf/on.el"))

(defun split-window-sensiblier (&optional window)
  "Wrapper over `split-window-sensibly' to make spliting smarter.
WINDOW is same as `split-window-sensibly'."
  (if (and window
	   (window-splittable-p window)
	   (window-splittable-p window t))
      (let ((width (window-width window))
	    (height (window-height window)))
	(with-selected-window window
	  (if (> width height)
	      (split-window-right)
	    (split-window-below))))
    (split-window-sensibly window)))
(setq split-window-preferred-function 'split-window-sensiblier
      split-width-threshold 120)

(set-face-attribute 'default nil
  :font "Maple Mono"
  :height 180
  :weight 'medium)
(set-face-attribute 'fixed-pitch nil
  :font "Maple Mono"
  :height 180
  :weight 'medium)

(setq display-line-numbers-type 'relative)
(setq-default display-line-numbers-current-absolute t
	      display-line-numbers-widen t
	      display-line-numbers-width 3)
(global-display-line-numbers-mode 1)
(column-number-mode 1)
(global-visual-line-mode t)

(defcustom display-line-numbers-exempt-modes
  '(vterm-mode eshell-mode shell-mode term-mode ansi-term-mode org-mode)
  "Major modes on which to disable line numbers."
  :group 'display-line-numbers
  :type 'list
  :version "green")

(defun display-line-numbers--turn-on ()
  "Turn on line numbers except for certain major modes.
Exempt major modes are defined in `display-line-numbers-exempt-modes'."
  (unless (or (minibufferp)
              (member major-mode display-line-numbers-exempt-modes))
    (display-line-numbers-mode)))

(global-hl-line-mode 1)

;; 100 is threshold
(setq scroll-conservatively 101
      scroll-margin 10)

;; Add a line indicator for programming modes
(add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)
;; Move line to 120 char, look at 'fill-column' again
(setq-default display-fill-column-indicator-column 120)

(setq use-short-answers t)

(setq ui-dialog-box nil)

(set-frame-parameter nil 'alpha-background 0.9)

(use-package hl-todo
  :ensure ( :version elpaca-latest-tag
	    :version-regexp "[.[:digit:]]+"
	    :depth 0)
  :hook ((prog-mode conf-mode text-mode) . hl-todo-mode)
  :config
  (setq hl-todo-require-punctuation t
	hl-todo-highlight-punctuation ":"
	hl-todo-keyword-faces
	'(("TODO" warning bold)
	  ("FIXME" error bold)
	  ("HACK" font-lock-constant-face bold)
	  ("DEPRECATED" font-lock-doc-face bold)
	  ("BUG" error bold)
	  ("NOTE" success bold)
	  ("XXX*" font-lock-constant-face bold))))

(use-package consult-todo
  :defer t)

(use-package which-key
  :hook (on-first-input . which-key-mode)
  :init
  (setq which-key-side-window-location 'bottom
	which-key-sort-order #'which-key-key-order-alpha
	which-key-sort-uppercase-first nil
	which-key-add-column-padding 1
	which-key-max-display-columns nil
	which-key-min-display-lines 6
	which-key-side-window-slot -10
	which-key-side-window-max-height 0.25
	which-key-idle-delay 1
	which-key-idle-secondary-delay 0
	which-key-max-description-length 25
	which-key-allow-imprecise-window-fit t
	which-key-separator " → " )
  ;(which-key-mode 1)
)

(use-package helpful
  :defer t
  :init
  (+general-global-help
    "f" #'helpful-function
    "m" #'helpful-macro
    "x" #'helpful-callable
    "b b" #'helpful-key
    "v" #'helpful-variable
    "p" #'helpful-at-point)
  (add-to-list 'display-buffer-alist '("\\*helpful"
				       (display-buffer-reuse-mode-window display-buffer-use-least-recent-window)
				       (dedicated . help)
				       (window-width . 0.5))))

(use-package vertico
  :hook (on-first-input . vertico-mode)
  :config
  ;; Use `consult-completion-in-region' if Vertico is enabled.
  ;; Otherwise use the default `completion--in-region' function.
  (setq completion-in-region-function
  	(lambda (&rest args)
  	  (apply (if vertico-mode
  		     #'consult-completion-in-region
  		   #'completion--in-region)
  		 args)))
  ;; Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  (setq read-extended-command-predicate
  	#'command-completion-default-include-p)
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
  		  (replace-regexp-in-string
  		   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
  		   crm-separator)
  		  (car args))
  	  (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)
  ;; Different scroll margin
  (setq vertico-scroll-margin 1)

  ;; Show more candidates
  ;; (setq vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  (setq vertico-resize nil)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  (setq vertico-cycle t)
  (general-def 'vertico-map
	"C-j" 'vertico-next
	"C-k" 'vertico-previous))

(use-package nerd-icons-completion
  :after marginalia
  :config
  (nerd-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package consult
  :defer t
  :init
  (+general-global-help
    "t" #'consult-theme
    "m" #'consult-man
    "i" #'consult-info
    "I" #'info)
  (general-def
    [remap switch-to-buffer] #'consult-buffer)
  (global-definer
    "i i" #'consult-yank-from-kill-ring
    "f g" #'consult-ripgrep)
  :config
  (setq consult-buffer-sources
	'(consult--source-hidden-buffer
	  consult--source-modified-buffer
	  consult--source-buffer
	  consult--source-project-buffer-hidden)))

(use-package marginalia
  :hook ((corfu-mode vertico-mode) . marginalia-mode))

(use-package corfu
  :hook (on-first-input . global-corfu-mode)
  :custom
  (corfu-auto t)                 ;; Enable auto completion
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  (corfu-preview-current 'const) ;; Disable current candidate preview
  (corfu-preselect 'prompt)      ;; Preselect the prompt
  (corfu-on-exact-match 'insert) ;; Configure handling of exact matches
  (corfu-scroll-margin 2)        ;; Use scroll margin
  (corfu-auto-delay 0.1)         ;; Time to wait until showign completions
  (corfu-auto-prefix 2)          ;; Minimum length for autocompletion to appear
  :config
  (setq text-mode-ispell-word-completion nil)
  (corfu-popupinfo-mode)
  (general-def 'corfu-map
    ;; No orderless, no separator
    ;; "<C-SPC>" 'corfu-insert-separator
    "<C-y>" 'corfu-insert
    "<tab>" nil
    "TAB" nil)
  (general-imap "<C-SPC>" 'completion-at-point))

(use-package nerd-icons-corfu
  :after corfu
  :init (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(use-package corfu-popupinfo
  :ensure nil
  :hook (corfu-mode . corfu-popupinfo-mode)
  :custom
  (corfu-popupinfo-delay '(0.5 . 0.5)))

(use-package cape
  :after corfu
  :config
  ;(setq-local completion-at-point-functions
	;      '(cape-file cape-dabbrev))
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file))

(use-package fussy
  :after (:any vertico corfu)
  :config
  (add-to-list 'completion-styles 'fussy)
  (setq
   ;; completion-styles '(fussy)
   ;; fussy-filter-fn 'fussy-filter-flex
   ;; For example, project-find-file uses 'project-files which uses
   ;; substring completion by default. Set to nil to make sure it's using
   ;; flx.
   completion-category-defaults nil
   completion-category-overrides nil))

;; (use-package fzf-native
;;   :straight
;;   (:repo "dangduc/fzf-native"
;;    :host github
;;    :files (:defaults "*.c" "*.h" "*.txt"))
;;   :init
;;   (setq fzf-native-always-compile-module t)
;;   :config
;;   (fzf-native-load-own-build-dyn))

(use-package flx-rs
  :after fussy
  :ensure
  (flx-rs :repo "jcs-elpa/flx-rs"
	  :fetcher github
	  :files (:defaults "bin"))
  :config
  (advice-add 'flx-score :override #'flx-rs-score)
  (setq fussy-score-fn 'fussy-flx-rs-score)
  (flx-rs-load-dyn))

(use-package treesit-auto
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(defun +org/dwim-at-point (&optional arg)
  "Do-what-I-mean at point.

If on a:
- checkbox list item or todo heading: toggle it.
- citation: follow it
- headline: cycle ARCHIVE subtrees, toggle latex fragments and inline images in
  subtree; update statistics cookies/checkboxes and ToCs.
- clock: update its time.
- footnote reference: jump to the footnote's definition
- footnote definition: jump to the first reference of this footnote
- timestamp: open an agenda view for the time-stamp date/range at point.
- table-row or a TBLFM: recalculate the table's formulas
- table-cell: clear it and go into insert mode. If this is a formula cell,
  recaluclate it instead.
- babel-call: execute the source block
- statistics-cookie: update it.
- src block: execute it
- latex fragment: toggle it.
- link: follow it
- otherwise, refresh all inline images in current tree."
  (interactive "P")
  (if (button-at (point))
      (call-interactively #'push-button)
    (let* ((context (org-element-context))
           (type (org-element-type context)))
      ;; skip over unimportant contexts
      (while (and context (memq type '(verbatim code bold italic underline strike-through subscript superscript)))
        (setq context (org-element-property :parent context)
              type (org-element-type context)))
      (pcase type
        ((or `citation `citation-reference)
         (org-cite-follow context arg))

        (`headline
         (cond ((memq (bound-and-true-p org-goto-map)
                      (current-active-maps))
                (org-goto-ret))
               ((and (fboundp 'toc-org-insert-toc)
                     (member "TOC" (org-get-tags)))
                (toc-org-insert-toc)
                (message "Updating table of contents"))
               ((string= "ARCHIVE" (car-safe (org-get-tags)))
                (org-force-cycle-archived))
               ((or (org-element-property :todo-type context)
                    (org-element-property :scheduled context))
                (org-todo
                 (if (eq (org-element-property :todo-type context) 'done)
                     (or (car (+org-get-todo-keywords-for (org-element-property :todo-keyword context)))
                         'todo)
                   'done))))
         ;; Update any metadata or inline previews in this subtree
         (org-update-checkbox-count)
         (org-update-parent-todo-statistics)
         (when (and (fboundp 'toc-org-insert-toc)
                    (member "TOC" (org-get-tags)))
           (toc-org-insert-toc)
           (message "Updating table of contents"))
         (let* ((beg (if (org-before-first-heading-p)
                         (line-beginning-position)
                       (save-excursion (org-backto-heading) (point))))
                (end (if (org-before-first-heading-p)
                         (line-end-position)
                       (save-excursion (org-end-of-subtree) (point))))
                (overlays (ignore-errors (overlays-in beg end)))
                (latex-overlays
                 (cl-find-if (lambda (o) (eq (overlay-get o 'org-overlay-type) 'org-latex-overlay))
                             overlays))
                (image-overlays
                 (cl-find-if (lambda (o) (overlay-get o 'org-image-overlay))
                             overlays)))
           (+org--toggle-inline-images-in-subtree beg end)
           (if (or image-overlays latex-overlays)
               (org-clear-latex-preview beg end)
             (org--latex-preview-region beg end))))

        (`clock (org-clock-update-time-maybe))

        (`footnote-reference
         (org-footnote-goto-definition (org-element-property :label context)))

        (`footnote-definition
         (org-footnote-goto-previous-reference (org-element-property :label context)))

        ((or `planning `timestamp)
         (org-follow-timestamp-link))

        ((or `table `table-row)
         (if (org-at-TBLFM-p)
             (org-table-calc-current-TBLFM)
           (ignore-errors
             (save-excursion
               (goto-char (org-element-property :contents-begin context))
               (org-call-with-arg 'org-table-recalculate (or arg t))))))

        (`table-cell
         (org-table-blank-field)
         (org-table-recalculate arg)
         (when (and (string-empty-p (string-trim (org-table-get-field)))
                    (bound-and-true-p evil-local-mode))
           (evil-change-state 'insert)))

        (`babel-call
         (org-babel-lob-execute-maybe))

        (`statistics-cookie
         (save-excursion (org-update-statistics-cookies arg)))

        ((or `src-block `inline-src-block)
         (org-babel-execute-src-block arg))

        ((or `latex-fragment `latex-environment)
         (org-latex-preview arg))

        (`link
         (let* ((lineage (org-element-lineage context '(link) t))
                (path (org-element-property :path lineage)))
           (if (or (equal (org-element-property :type lineage) "img")
                   (and path (image-type-from-file-name path)))
               (+org--toggle-inline-images-in-subtree
                (org-element-property :begin lineage)
                (org-element-property :end lineage))
             (org-open-at-point arg))))

        (`paragraph
         (+org--toggle-inline-images-in-subtree))

        ((guard (org-element-property :checkbox (org-element-lineage context '(item) t)))
         (let ((match (and (org-at-item-checkbox-p) (match-string 1))))
           (org-toggle-checkbox (if (equal match "[ ]") '(16)))))

        (_
         (if (or (org-in-regexp org-ts-regexp-both nil t)
                 (org-in-regexp org-tsr-regexp-both nil  t)
                 (org-in-regexp org-link-any-re nil t))
             (call-interactively #'org-open-at-point)
           (+org--toggle-inline-images-in-subtree
            (org-element-property :begin context)
            (org-element-property :end context))))))))

(use-builtin org
  :defer t
  :init
  (setq org-directory "~/Documents/org/"
	org-agenda-files (list org-directory
			       (expand-file-name "config.org" (file-name-directory user-init-file))))
  ;; please let me type <s, k thx bye
  ;; (add-hook 'org-mode-hook (lambda ()
  ;; 			     (progn
  ;; 			       (modify-syntax-entry ?< ".")
  ;; 			       (modify-syntax-entry ?> "."))))
  (+general-global-notes
    "f" (list (find-file-in-directory org-directory) :wk "Find note"))
  :config
  (setq org-hide-emphasis-markers t
	org-log-done 'time
	org-edit-src-content-indentation 0
	org-insert-heading-respect-content t
	org-pretty-entities t
	org-ellipsis "…"
	;; org-ellipsis "​"
	)
  (dolist (face '((org-level-1 . 1.25)
                  (org-level-2 . 1.20)
                  (org-level-3 . 1.15)
                  (org-level-4 . 1.10)
                  (org-level-5 . 1.05)
                  ;; (org-document-title . 1.3)
                  ;; (org-document-info-keyword . 1.1)
		  ))
    (set-face-attribute (car face) nil :height (cdr face) :weight 'semi-bold))
  (add-to-list 'display-buffer-alist '("\\*Org Src"
				       (display-buffer-pop-up-window)
				       (window-width . 0.6)))
  (global-leader
      :major-modes 'org-mode
      :keymaps 'org-mode-map
      "'" 'org-edit-special
      "t" 'org-set-tags-command
      "p" 'org-set-property
      "/" #'consult-org-heading
      "." #'consult-org-agenda
      "*" #'org-ctrl-c-star
      "-" #'corg-ctrl-c-minus
  
      "b" '(:ignore t :wk "tables")
      "b -" #'org-table-insert-hline
      "b a" #'org-table-align
      "b b" #'org-table-blank-field
      "b c" #'org-table-create-or-convert-from-region
      "b e" #'org-table-edit-field
      "b f" #'org-table-edit-formulas
      "b h" #'org-table-field-info
      "b s" #'org-table-sort-lines
      "b r" #'org-table-recalculate
      "b R" #'org-table-recalculate-buffer-tables
  
      "b d" '(:ignore t :wk "delete")
      "b d c" #'org-table-delete-column
      "b d r" #'org-table-kill-row
  
      "b i" '(:ignore t :wk "insert")
      "b i c" #'org-table-insert-column
      "b i h" #'org-table-insert-hline
      "b i r" #'org-table-insert-row
      "b i H" #'org-table-hline-and-move
  
      "b t" '(:ignore t :wk "toggle")
      "b t f" #'org-table-toggle-formula-debugger
      "b t o" #'org-table-toggle-coordinate-overlays
  
      "g" '(:ignore t :wk "goto")
      "g g" #'consult-org-heading
      "g G" #'consult-org-agenda
  
      "n" #'org-store-link
  
      "l" '(:ignore t :wk "links")
      "l i" #'org-id-store-link
      "l l" #'org-insert-link
      "l L" #'org-insert-all-links
      "l s" #'org-store-link
      "l S" #'org-insert-last-stored-link
      "l t" #'org-toggle-link-display
      )
  
  (general-define-key
   :major-modes t
   :keymaps 'org-mode-map
   :states 'normal
   [return] '+org/dwim-at-point))

(use-package org-auto-tangle
  :hook (org-mode . org-auto-tangle-mode))

(use-package toc-org
  :hook (org-mode . toc-org-mode))

(use-package org-appear
  :hook (org-mode . org-appear-mode)
  :init
  (add-hook 'org-mode-hook (lambda ()
			     (add-hook 'evil-insert-state-entry-hook
				       #'org-appear-manual-start
				       nil
				       t)
			     (add-hook 'evil-insert-state-exit-hook
				       #'org-appear-manual-stop
				       nil
				       t)))
  :config
  (setq org-appear-trigger 'manual
	  org-appear-autolinks t
	  org-appear-autoemphasis t))

(use-package org-modern
  :hook (org-mode . org-modern-mode)
  :hook (org-agenda-finalize . org-modern-mode)
  :config
  (setq org-modern-hide-stars " "))

(use-package magit
  :defer t
  :init
  (+general-global-git
    "s" '(magit :wk "Git status")
    "g" '(magit :wk "Git status"))
  :config
  (define-key transient-map [escape] #'transient-quit-one)
  (setq magit-diff-refine-hunk t
	magit-display-buffer-function #'magit-display-buffer-fullframe-status-v1))

(use-package magit-todos
  :after magit
  :init
  (add-to-list 'safe-local-variable-values '(magit-todos-scanner . magit-todos--scan-with-git-grep))
  :config
  (general-def 'magit-todos-section-map
    "k" nil
    "SPC" nil)
  (general-def 'magit-todos-item-section-map
    "k" nil
    "SPC" nil)
  (magit-todos-mode 1))

(defun yadm ()
  (interactive)
  (let ((enable-remote-dir-locals t))
    (magit-status "/yadm::")))

(+general-global-git
 "p" '(yadm :wk "Open yadm repo"))

(with-eval-after-load 'tramp
  (add-to-list 'tramp-methods
               '("yadm"
		 (tramp-login-program "yadm")
		 (tramp-login-args (("enter")))
		 (tramp-login-env (("SHELL") ("/bin/sh")))
		 (tramp-remote-shell "/bin/sh")
		 (tramp-remote-shell-args ("-c")))))

(use-package git-commit
  :defer t
  :config
  (add-hook 'git-commit-setup-hook #'evil-insert-state))

(use-package diff-hl
  :hook (find-file . diff-hl-mode)
  :hook (vc-dir-mode . diff-hl-dir-mode)
  :hook (dired-mode . diff-hl-dired-mode)
  :hook (diff-hl-mode . diff-hl-flydiff-mode)
  :config
  (setq vc-handled-backends '(Git Hg))
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))

(use-builtin tramp
  :defer t
  :config
  (setq tramp-persistency-file-name (expand-file-name "tramp/" emacs-cache-directory)
	tramp-default-remote-shell "/usr/bin/bash"))

(use-builtin eglot
  :hook ((rust-ts-mode . eglot-ensure))
  :init
  (add-hook 'eglot-managed-mode-hook (lambda () (eglot-inlay-hints-mode -1)))
  :config
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)
  (setopt eglot-autoshutdown t
	  eglot-events-buffer-config '(:size 0)
	  eglot-ignored-server-capabilities '(:documentOnTypeFormattingProvider))
  (general-nmap
    :keymaps 'eglot-mode-map
    "SPC c h" #'eglot-inlay-hints-mode))

(use-package lsp-ui
  :after lsp-mode
  :config
  (setq lsp-ui-sideline-show-hover t
	lsp-ui-sideline-show-diagnostics t
	lsp-ui-sideline-show-symbol t
	lsp-ui-sideline-show-code-actions t
	lsp-ui-sideline-delay 0.5))

;; Configure Tempel
(use-package tempel
  :defer t
  ;; Require trigger prefix before template name when completing.
  :custom
  (tempel-path (expand-file-name "/templates/*/*.eld" user-emacs-directory))
  ;; (tempel-trigger-prefix "<")

  :init
  ;; Setup completion at point
  (defun tempel-setup-capf ()
    ;; Add the Tempel Capf to `completion-at-point-functions'.
    ;; `tempel-expand' only triggers on exact matches. Alternatively use
    ;; `tempel-complete' if you want to see all matches, but then you
    ;; should also configure `tempel-trigger-prefix', such that Tempel
    ;; does not trigger too often when you don't expect it. NOTE: We add
    ;; `tempel-expand' *before* the main programming mode Capf, such
    ;; that it will be tried first.
    (setq-local completion-at-point-functions
                (cons #'tempel-complete
                      completion-at-point-functions)))

  ;; (defun +tempel-maybe-expand ()
  ;;   "Call tempel-expand if cursor is at a snippet, alternatively delegate TAB to the next keymap."
  ;;   (interactive)
  ;;   (let ((keymaps minor-mode-map-alist))
  ;;     )
  ;;   )

  ;; (general-imap "TAB" #'tempel-expand)
  (general-define-key :keymaps 'tempel-map
		      :states 'insert
		      "C-l" #'tempel-next
		      "C-h" #'tempel-previous
		      "TAB" #'tempel-next
		      "S-TAB" #'tempel-previous)

  (add-hook 'conf-mode-hook 'tempel-setup-capf)
  (add-hook 'prog-mode-hook 'tempel-setup-capf)
  (add-hook 'text-mode-hook 'tempel-setup-capf)

  ;; Optionally make the Tempel templates available to Abbrev,
  ;; either locally or globally. `expand-abbrev' is bound to C-x '.
  ;; (add-hook 'prog-mode-hook #'tempel-abbrev-mode)
  ;; (global-tempel-abbrev-mode)
)

;; Optional: Add tempel-collection.
;; The package is young and doesn't have comprehensive coverage.
(use-package tempel-collection
  :after tempel)

(use-package lsp-snippet-tempel
  :after eglot
  :ensure (lsp-snippet-tempel :host github
			:repo "svaante/lsp-snippet")
  :config
  (when (featurep 'lsp-mode)
    ;; Initialize lsp-snippet -> tempel in lsp-mode
    (lsp-snippet-tempel-lsp-mode-init))
  (when (featurep 'eglot)
    ;; Initialize lsp-snippet -> tempel in eglot
    (lsp-snippet-tempel-eglot-init)))

(use-package flycheck
  :hook (on-first-buffer . global-flycheck-mode)
  :config
  (defun flycheck-may-use-echo-area-p ()
    nil)
  (setopt flycheck-emacs-lisp-load-path 'inherit))

;; (use-package flycheck-inline
;;   :hook (flycheck-mode . flycheck-inline-mode))

(use-package sideline
  :hook (flycheck-mode . sideline-mode))

(use-package sideline-flycheck
  :hook (flycheck-mode . sideline-flycheck-setup)
  :config
  (add-to-list 'sideline-backends-right '(sideline-flycheck))
  (setopt sideline-flycheck-max-lines 5))

(use-builtin eldoc
  :config
  (setq eldoc-echo-area-use-multiline-p nil))

(use-builtin emacs-lisp-mode
  :defer t
  :init
  (global-leader
    :major-modes 'emacs-lisp-mode
    :keymaps 'emacs-lisp-mode-map
    "e" '(:ignore t :wk "Eval")
    "e b" 'eval-buffer
    "e r" 'eval-region
    "e l" 'pp-eval-last-sexp
    "e d" 'eval-defun))

(use-package rust-mode
  :defer t
  :init
  (setq rust-mode-treesitter-derive t))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package aggressive-indent
  :hook (emacs-lisp-mode . aggressive-indent-mode))

(use-builtin project
  :init
  (+general-global-menu! "projects" "p"
    "p" #'project-switch-project
    "f" #'project-find-file)
  (setq project-list-file (expand-file-name "projects" emacs-cache-directory))
  :config
  (add-to-list 'project-switch-commands '(magit-project-status "Magit" ?m)))

(use-package elfeed
  :defer t
  :init
  (add-hook 'elfeed-search-mode-hook #'elfeed-update)
  (+general-global-application "e" 'elfeed))
  :config
  (setq elfeed-search-filter "@6-month-ago"
	elfeed-db-directory (expand-file-name "elfeed" emacs-local-directory))

(use-package elfeed-org
  :after elfeed
  :config
  (elfeed-org)
  (setq rmh-elfeed-org-files `(,(expand-file-name "elfeed.org" org-directory))))

(use-builtin transient
  :defer t
  :config
  (let ((transient-folder (expand-file-name "transient/" emacs-cache-directory)))
	(setq transient-history-file (expand-file-name "history" transient-folder))
	(setq transient-levels-file (expand-file-name "levels" transient-folder))
	(setq transient-values-file (expand-file-name "values" transient-folder))))

(use-package embark
  :defer t
  :init
  ;; (setq prefix-help-command #'embark-prefix-help-command
  ;; 	which-key-use-C-h-commands nil)
  (global-definer "e" #'embark-act)
  ;; (+general-global-help "b b" #'embark-act)
  (general-def
    "C-;" #'embark-dwim
    "C-." #'embark-act
    [remap describe-bindings] #'embark-bindings)
  :config
  (defun sudo-find-file (file)
    "Open FILE as root."
    (interactive "FOpen file as root: ")
    (when (file-writable-p file)
      (user-error "File is user writeable, aborting sudo"))
    (find-file (if (file-remote-p file)
                   (concat "/" (file-remote-p file 'method) ":"
                           (file-remote-p file 'user) "@" (file-remote-p file 'host)
                           "|sudo:root@"
                           (file-remote-p file 'host) ":" (file-remote-p file 'localname))
		 (concat "/sudo:root@localhost:" file))))
  ;;   (defun embark-which-key-indicator ()
  ;;     "An embark indicator that displays keymaps using which-key.
  ;; The which-key help message will show the type and value of the
  ;; current target followed by an ellipsis if there are further
  ;; targets."
  ;;     (lambda (&optional keymap targets prefix)
  ;;       (if (null keymap)
  ;;           (which-key--hide-popup-ignore-command)
  ;; 	(which-key--show-keymap
  ;; 	 (if (eq (plist-get (car targets) :type) 'embark-become)
  ;;              "Become"
  ;;            (format "Act on %s '%s'%s"
  ;;                    (plist-get (car targets) :type)
  ;;                    (embark--truncate-target (plist-get (car targets) :target))
  ;;                    (if (cdr targets) "…" "")))
  ;; 	 (if prefix
  ;;              (pcase (lookup-key keymap prefix 'accept-default)
  ;;                ((and (pred keymapp) km) km)
  ;;                (_ (key-binding prefix 'accept-default)))
  ;;            keymap)
  ;; 	 ;;       V this nil controls paging, t to disable
  ;;          ;; sadly a hacky way that doesn't fully work
  ;;          ;; https://github.com/oantolin/embark/issues/647
  ;; 	 nil nil nil (lambda (binding)
  ;;                        (not (string-suffix-p "-argument" (cdr binding))))))))

  ;;   (setq embark-indicators
  ;; 	'(embark-which-key-indicator
  ;; 	  embark-highlight-indicator
  ;; 	  embark-isearch-highlight-indicator))

  ;;   (defun embark-hide-which-key-indicator (fn &rest args)
  ;;     "Hide the which-key indicator immediately when using the completing-read prompter."
  ;;     (which-key--hide-popup-ignore-command)
  ;;     (let ((embark-indicators
  ;;            (remq #'embark-which-key-indicator embark-indicators)))
  ;;       (apply fn args)))

  ;;   (advice-add #'embark-completing-read-prompter
  ;;               :around #'embark-hide-which-key-indicator)
  (general-def 'embark-file-map
    "S" #'sudo-find-file))

(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook (embark-collect-mode . consult-preview-at-point-mode))

(use-package eat
  :ensure (eat
	   :host codeberg
	   :repo "akib/emacs-eat"
	   :files ("*.el" ("term" "term/*.el") "*.texi"
		   "*.ti" ("terminfo/e" "terminfo/e/*")
		   ("terminfo/65" "terminfo/65/*")
		   ("integration" "integration/*")
		   (:exclude ".dir-locals.el" "*-tests.el")))
  :defer t
  :init
  (+general-global-projects "t" #'eat-project)
  (general-nmap "SPC o t" #'toggle-eat)
  (general-nmap "SPC o T" #'eat-here)

  (defun toggle-eat ()
    (interactive)
    (require 'eat)
    (let ((buffer (get-buffer "*eat*"))
	  (eat-query-before-killing-running-terminal nil))
      (if buffer
	  (kill-buffer buffer)
	(eat))))

  (defun eat-here ()
    (interactive)
    (require 'eat)
    (let (display-buffer-alist)
      (eat)))

  :config
  ;; (setq eat-query-before-killing-running-terminal nil)
  (add-to-list 'display-buffer-alist '("\\*eat\\*"
				       (display-buffer-in-side-window)
				       (side . bottom)
				       (window-height . 0.3))))
