;;; early-init.el --- Emacs pre package.el & GUI configuration -*- lexical-binding: t; -*-
;;; Code:

(defvar default-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

(add-hook 'elpaca-after-init-hook
	  (lambda () (setq file-name-handler-alist default-file-name-handler-alist)))

(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 1)

(defun +gc-after-focus-change ()
  "Run GC when frame loses focus."
  (run-with-idle-timer
   2 nil
   (lambda () (unless (frame-focus-state) (garbage-collect)))))

(defun +reset-init-values ()
  (run-with-idle-timer
   1 nil
   (lambda ()
     (setq file-name-handler-alist default-file-name-handler-alist
           gc-cons-percentage 0.1
           gc-cons-threshold (* 16 1024 1024))
     (when (boundp 'after-focus-change-function)
       (add-function :after after-focus-change-function #'+gc-after-focus-change)))))

(with-eval-after-load 'elpaca
  (add-hook 'elpaca-after-init-hook '+reset-init-values))

(setq frame-inhibit-implied-resize t
      frame-resize-pixelwise t
      visual-bell nil)
(modify-all-frames-parameters '((ns-transparent-titlebar . t)
				(vertical-scroll-bars . nil)
				(horizontal-scroll-bars . nil)
				(menu-bar-lines . 0)
				(tool-bar-lines . 0)))
(set-fringe-mode 10)
(tooltip-mode -1)

(setq inhibit-compacting-font-caches t)

(defvar emacs-cache-directory (expand-file-name "emacs/" (or (getenv "XDG_CACHE_HOME") "~/.cache")))
(defvar emacs-local-directory (expand-file-name "emacs/" (or (getenv "XDG_DATA_HOME") "~/.local/share")))
(defvar emacs-state-directory (expand-file-name "emacs/" (or (getenv "XDG_STATE_HOME") "~/.local/share/state")))

(startup-redirect-eln-cache emacs-cache-directory)

(make-directory (expand-file-name "backups/" emacs-cache-directory) t)
(setq backup-directory-alist `(("." . ,(expand-file-name "backups/" emacs-cache-directory))))

(make-directory (expand-file-name "auto-saves/" emacs-cache-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "auto-saves/sessions/" emacs-cache-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "auto-saves/" emacs-cache-directory) t)))

(setq create-lockfiles nil)

;; (setq byte-compile-warnings '(not obsolete))
;; (setq warning-suppress-log-types '((comp) (bytecomp)))
(setq native-comp-async-report-warnings-errors 'silent)
(setq inhibit-startup-echo-area-message (user-login-name)
      inhibit-startup-message t)

(setq +benchmark (member "--benchmark" command-line-args))
(setq command-line-args (delete "--benchmark" command-line-args))

(setq package-enable-at-startup nil)

(provide 'early-init)
;;; early-init.el ends here
