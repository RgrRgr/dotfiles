local wezterm = require 'wezterm';
local act = wezterm.action

return {
	-- font = wezterm.font("monospace"),
	font = wezterm.font_with_fallback {
		{
			-- Sadly wezterm can't deal with fontconfigs config
			-- Have to configure it twice
			family = "Maple Mono",
			harfbuzz_features = { "ss01", "ss03", "ss04" },
		},
		"Symbols Nerd Font Mono"
	},
	font_size = 18,
	color_scheme = "AyuDark (Gogh)",
	-- color_scheme = "Tomorrow Night",
	-- color_scheme = "carbonfox",
	colors = {
		cursor_fg = "black",
		--foreground = "#b0bec5",
	},
	enable_tab_bar = false,
	keys = {
		{ key = "v", mods = "SHIFT|CTRL", action = act.PasteFrom("Clipboard") },
	},
	window_close_confirmation = "NeverPrompt",
	skip_close_confirmation_for_processes_named = {
		"bash", "sh", "zsh", "fish", "tmux", "qalc", "ranger"
	}
}
