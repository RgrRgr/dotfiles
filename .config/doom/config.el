;;; -*- lexical-binding: t; -*-

(setq user-full-name "Roger Roger"
      user-mail-address "me@rogerrogert.de")

(setq projectile-project-search-path '(("~/Projects" . 2)
                                       ("~/Git" . 1)))

(setq +lookup-provider-url-alist
      '(("Doom Emacs issues" "https://github.com/hlissner/doom-emacs/issues?q=is%%3Aissue+%s")
        ("DuckDuckGo"        +lookup--online-backend-duckduckgo "https://duckduckgo.com/?q=%s")
        ("StackOverflow"     "https://stackoverflow.com/search?q=%s")
        ("Arch Wiki"         "https://wiki.archlinux.org/index.php?search=%s&title=Special%3ASearch&wprov=acrw1")
        ("Gentoo Wiki"       "https://wiki.gentoo.org/index.php?search=%s")
        ("Github"            "https://github.com/search?ref=simplesearch&q=%s")
        ("Youtube"           "https://youtube.com/results?aq=f&oq=&search_query=%s")
        ("MDN"               "https://developer.mozilla.org/en-US/search?q=%s")
        ("Wikipedia"         "https://wikipedia.org/search-redirect.php?language=en&go=Go&search=%s")
        ("Rust Docs"         "https://doc.rust-lang.org/std/?search=%s")))

(setq evil-vsplit-window-right t
      evil-split-window-below t)

(setq evil-want-fine-undo t)

(setq evil-ex-substitute-global t)

;(setq doom-scratch-initial-major-mode 'lisp-interaction-mode)

(setq vterm-kill-buffer-on-exit t)

(setq vterm-always-compile-module t)

(setq vterm-shell "/bin/zsh")

(after! vterm
  (add-to-list 'vterm-tramp-shells '("ssh" "/bin/zsh")))

(setq orderless-matching-styles '(orderless-literal
                                orderless-flex
                                ;; orderless-regexp
                                ))

(setq select-enable-clipboard nil)

(setq evil-snipe-scope 'visible
      evil-snipe-repeat-scope 'visible)

(after! avy
  (setq avy-orders-alist
      '((avy-goto-char         . avy-order-closest)
        (avy-goto-char-in-line . avy-order-closest)
        (avy-goto-char-2       . avy-order-closest)
        (avy-goto-char-2-above . avy-order-closest)
        (avy-goto-char-2-below . avy-order-closest)
        (avy-goto-word-0       . avy-order-closest)
        (avy-goto-word-0-above . avy-order-closest)
        (avy-goto-word-0-below . avy-order-closest)
        (avy-goto-word-1       . avy-order-closest)
        (avy-goto-word-1-above . avy-order-closest)
        (avy-goto-word-1-above . avy-order-closest))))

(setq gc-cons-threshold 20000000)

(add-to-list 'auto-mode-alist '("\.conf##" . conf-mode))

(advice-add '+emacs-lisp-truncate-pin
            :override (lambda () "Don't ellipsise text" ()))

(when (version< "29.0.50" emacs-version)
  (setq pixel-scroll-precision-large-scroll-height 120.0
        pixel-scroll-precision-interpolate-page 't)
  (pixel-scroll-precision-mode)

(defun lines-to-pixels (line)
  (cl-typecase line
    (integer (* line (line-pixel-height)))
    ((or null (member -))
     (- (window-text-height nil 't)
        (* next-screen-context-lines (line-pixel-height))))
    (t (line-pixel-height))))

(defadvice! my-smooth-scroll-up (&optional arg)
  :override 'scroll-up
  (pixel-scroll-precision-interpolate (- (lines-to-pixels arg)) nil 1))

(defadvice! my-smooth-scroll-down (&optional arg)
  :override 'scroll-down
  (pixel-scroll-precision-interpolate (lines-to-pixels arg) nil 1)))

(setq doom-font (font-spec :family "Monospace" :size 25)
      doom-variable-pitch-font (font-spec :family "Sans" :size 25))

(setq-hook! org-mode prettify-symbols-alist '(("#+begin_src" . "»")
                                              ("#+end_src" . "«")))

(custom-set-faces!
  '(font-lock-comment-face :slant italic))

;(setq doom-theme 'doom-tomorrow-night)
(setq doom-theme 'doom-ayu-dark)
;(setq doom-theme 'doom-molokai)

(custom-theme-set-faces! 'doom-ayu-dark
  `(powerline-active2 :background "#202329" :inherit ,(doom-color 'mode-line))
  `(corfu-default :background ,(doom-color 'common-bg))
  `(corfu-current :background ,(doom-color 'ui-line))
  `(line-number :foreground ,(doom-color 'comments))
  `(evil-snipe-first-match-face :inherit ,(doom-color 'isearch))
  `(tree-sitter-hl-face:variable :foreground "#ff8f40" :inherit 'font-lock-variable-name-face))

(setq display-line-numbers-type 'relative)

(setq scroll-margin 5)

(set-frame-parameter nil 'alpha-background 0.9)

(after! evil-snipe
  (evil-define-key* '(motion normal visual) evil-snipe-local-mode-map
    "s" nil
    "S" nil))

(use-package! evil-surround
  :commands (global-evil-surround-mode
             evil-surround-edit
             evil-Surround-edit
             evil-surround-region)
  :config (global-evil-surround-mode -1))
;(setq global-evil-surround-mode 0)
;; (after! evil-surround
;;   (evil-define-key* 'visual evil-surround-mode-map "S" nil)
;;   (evil-define-key* 'motion evil-surround-mode-map "s" nil)
;;   (evil-define-key* 'operator evil-surround-mode-map "s" nil
;;     "S" nil))

;; (evil-define-key 'motion 'evil-motion-state-map
;;   "s" #'evil-avy-goto-char-2-below
;;   "S" #'evil-avy-goto-char-2-above)
;; (evil-global-set-key 'motion "s" #'evil-avy-goto-char-2)
;; (evil-global-set-key 'motion "S" #'evil-avy-goto-char-2)
(map! :nvm "s" #'evil-avy-goto-char-2-below
      :nvm "S" #'evil-avy-goto-char-2-above)
(map! :map 'evilem-map
      :nvm "s" #'evil-avy-goto-char-2-below
      :nvm "S" #'evil-avy-goto-char-2-above)

(map!
  "<XF86Paste>" 'clipboard-yank
  "<Paste>" 'clipboard-yank
  "s-<XF86Paste>" 'clipboard-yank

  "<XF86Cut>" 'clipboard-kill-region
  "<Cut>" 'clipboard-kill-region
  "s-<XF86Cut>" 'clipboard-kill-region

  "<XF86Copy>" 'clipboard-kill-ring-save
  "<86Copy>" 'clipboard-kill-ring-save
  "s-<XF86Copy>" 'clipboard-kill-ring-save
  :leader :desc "Copy to system clipboard" "y" 'clipboard-kill-ring-save)

(map! :desc "Redo" :n "U" 'evil-redo)

(setq org-directory "~/Documents/org/"
      org-roam-directory (expand-file-name "roam/" org-directory))

(setq org-hide-emphasis-markers t
      org-log-done 'time
      org-journal-date-format "%a, %Y-%m-%d"
      org-journal-file-format "%Y-%m-%d.org")

(add-hook 'org-mode-hook (lambda ()
                            (setq-local display-line-numbers nil)))

(after! org
  (setq org-startup-indented nil))

(map! :after org
      :map org-mode-map
      :nv "C-k" 'org-metaup
      :nv "C-j" 'org-metadown
      ;:nv "s-k" 'org-metaup
      ;:nv "s-j" 'org-metadown
      )

(use-package! org-appear
  :hook (org-mode . org-appear-mode)
  :init
  (add-hook 'org-mode-hook (lambda ()
                             (add-hook! 'evil-insert-state-entry-hook :local #'org-appear-manual-start)
                             (add-hook! 'evil-insert-state-exit-hook :local #'org-appear-manual-stop)))
  :config
  (setq org-appear-trigger 'manual
        org-appear-autolinks t
        org-appear-autoemphasis t))

(after! evil-org
  (defun evil-org-src-save (&optional file bang)
    (interactive)
    (when file
      (evil-save file bang))
    (org-edit-src-save))
  (defun evil-org-src-save-and-close (&optional file bang)
    (interactive)
    (evil-org-src-save file bang)
    (org-edit-src-exit))
  (define-key org-src-mode-map [remap evil-write] 'evil-org-src-save)
  (define-key org-src-mode-map [remap evil-save-and-close] 'evil-org-src-save-and-close)
  (define-key org-src-mode-map [remap evil-save-modified-and-close] 'evil-org-src-save-and-close))

;; FIXME auto expands snippets in rust iter.for
(after! corfu
  (map! :map corfu-map
        ;"<C-space>" #'corfu-separator
        "<tab>" nil
        "TAB" nil)
  (setq corfu-preselect 'prompt
        corfu-preview-current 'const
        corfu-auto-delay 0
        corfu-scroll-margin 1
        corfu-count 10
        corfu-auto-prefix 1
        corfu-right-margin-width 1.0
        corfu-min-width 30)
  (add-hook! 'evil-insert-state-exit-hook #'corfu-quit))

(after! company
  (setq company-idle-delay 0
        company-minimum-prefix-length 1
        company-selection-wrap-around t)
  (setq company-backends '(company-files company-capf company-yasnippet))
  (set-company-backend! '(company-files company-capf company-yasnippet))
  (add-hook! 'evil-insert-state-exit-hook #'company-abort))

(map! :after company
      ;<tab> and "TAB" are *NOT* interchangable
      :map company-tng-map
       "<return>" #'company-complete-selection
       "RET" #'company-complete-selection
       "<tab>" #'yas-next-field-or-maybe-expand
       "TAB" #'yas-next-field-or-maybe-expand
      :map company-active-map
       "<return>" #'company-complete-selection
       "RET" #'company-complete-selection
       "<tab>" #'yas-next-field-or-maybe-expand
      "TAB" #'yas-next-field-or-maybe-expand)

(after! lsp-ui-doc
  (setq lsp-ui-doc-show-with-cursor t
        lsp-ui-doc-delay 1
        lsp-signature-doc-lines 0))

(map! :after dired
      :map dired-mode-map
      :n "h" #'dired-up-directory
      :n "l" #'dired-find-file)

;; (after! dirvish
;;   (dirvish-layout-toggle))

(map! :map dired-mode-map :desc "Toggle multipane layout" "z P" #'dirvish-layout-toggle)

(map! :leader :desc "Open project bar" :prefix "o" "p" #'dirvish-side)

(after! elfeed
  (add-hook 'elfeed-search-mode-hook #'elfeed-update)
  (setq elfeed-search-filter "@6-month-ago"
        elfeed-goodies/entry-pane-size 0.5))

;; Add elfeed shortcut
(map! :leader :prefix "o" :desc "Open Elfeed" "e" #'elfeed)

(defun my/elfeed-db-remove-entry (id)
  "Removes the entry for ID"
  (avl-tree-delete elfeed-db-index id)
  (remhash id elfeed-db-entries))

(defun my/elfeed-search-remove-selected ()
  "Remove selected entries from database"
  (interactive)
  (let* ((entries (elfeed-search-selected))
         (count (length entries)))
    (when (y-or-n-p (format "Delete %d entires?" count))
      (cl-loop for entry in entries
               do (my/elfeed-db-remove-entry (elfeed-entry-id entry)))))
  (elfeed-search-update--force))

(use-package! tramp-yadm
  :after (:any tramp projectile)
  :init
  (tramp-yadm-register)
  ;; TODO this isn't working properly yet, should be fixed upstream
  :config
  (setq +vc-gutter-in-remote-files t))

(defun yadm-status ()
  "Invoke magit on the yadm repo"
  (interactive)
  (require 'tramp-yadm)
  (magit-status-setup-buffer "/yadm::~")
  (setq-local magit-git-executable (executable-find "yadm"))
  (setq-local magit-remote-git-executable (executable-find "yadm")))
;; Map said function to SPC g p
(map! :leader :desc "Open yadm status" "g p" #'yadm-status)

(advice-add 'consult-recent-file
            :before (lambda (&rest _r)
                      "tramp-yadm needs to load in case we have a /yadm:: entry"
                      (require 'tramp-yadm)))

(use-package! org-modern
  :after org
  :init
  (setq org-modern-hide-stars " ")
  :config
  (global-org-modern-mode))

(map! :after image
      :map image-mode-map
      :desc "Close buffer" "q" #'kill-buffer
                )
