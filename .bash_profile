### Paths ###
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_STATE_HOME=$HOME/.local/state
#export XDG_CACHE_HOME=$HOME/.cache
export XDG_CACHE_HOME=$HOME/.local/cache
export PATH="$HOME/.local/bin:$PATH"
#export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$(id -u)/bus"

### Clean home ###
#Rust
export CARGO_INSTALL_ROOT="$HOME/.local/"
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export CARGO_HOME="$XDG_DATA_HOME"/cargo

#Ocaml
export OPAMROOT="$XDG_DATA_HOME/opam"

#Go
export GOPATH="$XDG_DATA_HOME"/go
export GOMODCACHE="$XDG_CACHE_HOME"/go/mod

#Java
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java

#Js/npm
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc

### Look and feel ###
export QT_QPA_PLATFORMTHEME=qt5ct
export GTK_THEME=Materia-dark
export BAT_THEME=base16
export XCURSOR_THEME=Adwaita
export XCURSOR_SIZE=24

if [ "$(tty)" = "/dev/tty1" ]; then
    exec Hyprland
fi

if [ -f ~/.bashrc ]; then
	source ~/.bashrc
fi
